# HDF5 Reader/Writer Design Document

Python module standard interface to our Curated HDF5 files

The idea is that we are going to treat the smallest unit of data as a Pixel
object which contains all the information about a given Pixel including the data,
row, col, and methods to manipulate data from an individual pixel.

## Design Questions

1. Once H5 file created is the file used once or multiple times as time goes by?

2. what's the structure of the contents? 

hierarchical? 

relational wise? 

3. For a given HDF5 file, is there any relationship with subsequent files? 

4. What parameters do we use to get the file contents?

5. For the file that we are going to write is it the same file format as the file 
   that is read or different?

# HDF5 File Structure

A short intro to what data is available in the HDF5 File.

Only show relevent details.

The pixel_info folder has a series of pixels which are numbered sequentially
with integers starting with 0 and up.

```
HDF5File -> pixel_info -> pixel[0], pixel[1], ..., -> col, row, raw_signal gap_size, material, tip_shape
```

## HDF5 File Data

1. pixel[X] - Folder with other pixel data

2. col - column number the pixel is from

3. row - column number the pixel is from

4. raw_signal - the data we are interested in: an array of uint8

# Pixel Type Classes

## Pixel

Represents all the pixel data in a Python object.

### Pixel Variables/Properties

1. x - position information

2. y - position information

3. idx - number the pixel represents in a series of pixels

4. data - data contained inside a given pixel, an array of unit8

### Pixel Methods

1. getData(start_idx, stop_idx) - Get the array or part of the array. No arguments will return the entire array.

## PixelList

Represents a Python List of Pixel objects.

### PixelList Methods

1. operator[] - Allows PixelList to be treated as an array. Will return the given Pixel given a number.

2. __iter__() returns the PixelList as an Pyhthon Iterator.

3. __next__() - returns the next Pixel in order. Raises StopIteration exception when the list is exhausted.

4. idxes - an array of all idx variables from each Pixel. NOTE: idxes won't necessarily be contiguous.

DESIGN QUESTION - Are there any transformations we'd do with all Pixels?

# Pixel IO Classes

## PixelReader Class

### PixelReader Abstract Methods 

1. read(row, col) -> Pixel

Reads Pixel Data 

2. read() -> PixelList

### PixelReader Properties

1. Path - Which storage "container" the data is in. Could be the name of a
   database or could be a directory path or some other kind of identifier than
   contains many PixelLists.

2. Key - Which specific group are we going to read a PixelList from.

## HDF5PixelReader Class

Implements methods of PixelReader using HDF5 backend.

## CloudPixelReader Class

Implements methods of PixelReader using Cloud backend.

## PixelWriter Class

Writes HDF5 Pixel Data Class. Not well fleshed out. Need more information on the file to be written.

### Methods 

1. write() 

Writes Pixel Data 

## HDF5PixelWriter Class

Implements methods of PixelWriter using HDF5 backend.
