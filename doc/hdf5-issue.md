issue.md

In order to:

Provide a uniform interface into pixel data in HDF5 classes.

We will:

Create classes for dealing with Pixel data.

This is done when:

Our HDF5 classes are integrated into rbtdata.