#!/usr/bin/env python3

"""Given a yaml file, create a H5 file from our raw data."""

import sys
from collections import namedtuple
from pathlib import Path

import frolix5.heirloom as heirloom

import rbtdata.expr.gen3.event as event
import rbtdata.expr.gen3.search as search
from rbtdata.expr.h5.experiment_writer import H5ExperimentWriter
from rbtdata.expr.h5.expr import H5Experiment
from rbtdata.expr.h5.expr import H5Phase
from rbtdata.io.pixel import Pixel
from rbtdata.signal.tdms import SignalReader

import yaml

if len(sys.argv) > 1:
    CONFIG = sys.argv[1]
else:
    CONFIG = "autobahn.yml"

""""
TODO:

1. Get pixel row, col from database instead of Excel file

2. All calls to heirloom code should be gone
"""


with open(CONFIG) as config_file:
    config = yaml.safe_load(config_file)

# By default, we'll pull in the sheet called "Of Interest"
# Which is the curated pixels which look 100% correct.
# If rank2 is true, we'll pull in the "Maybe" pixels
# as well.
sheets = ["Of Interest"]
try:
    if config["rank2"]:
        sheets.append("Maybe")
        print("Processing rank 2 pixels as well")
except KeyError:
    print(f"WARN: [HARMLESS] rank2 flag not in set in {CONFIG}")
    print("should be:")
    print("")
    print("rank2: False")
    print("")

out_prefix = (
    "/mnt/remslocal/Users/signal_processing/machine_learning/H5/in_process"
)
try:
    if config["out_prefix"]:
        out_prefix = config["out_prefix"]
except KeyError:
    print(f"WARN: [HARMLESS] rank2 flag not in set in {CONFIG}")
    print("")
    print(f"defaulting to {out_prefix}")
out_prefix = Path(out_prefix)
out_prefix.mkdir(parents=True, exist_ok=True)

out_path = out_prefix / Path(f'{config["chip"]}_out.h5')

# Delete the H5 if it exists.
# If not, keep rolling.
try:
    out_path.unlink()
except FileNotFoundError:
    pass

# Given our chip information, create a Signal Reader
sr = SignalReader(config["chip"])


exp: H5Experiment = H5Experiment()

paths = search.chip_query(config["chip"])
exp_dir = heirloom.get_exp_dir(paths)
filename = heirloom.get_filename(str(exp_dir))
log_path = search.event_log(
    exp_dir
)  # use the exp_dir to get the path to the log file
exp._phases = event.phases(log_path, True)

# Jam in our parameters for now.
# We'll read this from our Experiment file in the final version.
exp._params = config["exp_info"]
# Add chip_name to exp_info.
exp._params["chip_name"] = config["chip"]
# Ensure out nucleotide is present and in a valid form:
assert heirloom.validate_nucleotide(exp._params["nucleotide"])

reagents = config["reagents"]
# namedtuple is read only therefore,
# we need to rewrite it to add the reagents field
phase_plus = namedtuple("PhasePlus", event.Phase._fields + ("params",))
for i in range(0, len(exp._phases)):
    new_phase = H5Phase(exp._phases[i].start, exp._phases[i].end, "phase")
    new_phase._params = exp._phases[i]._asdict()

    # Machine learning wants the real phase name
    # which is often in the message.
    # Only include phase_name if it exists.
    tmp = new_phase._params["message"].split(" ")
    if len(tmp) > 0:
        new_phase._params["phase_name"] = tmp[0].upper()

    if exp._phases[i].phase != "IGNORE":
        try:
            new_phase._params.update({"reagent_group": reagents[i]})
        except KeyError:
            pass
    exp._phases[i] = new_phase

# read a List of row, col
# TODO GET FROM DATABASE
pixel_dict = {}
data_path = exp_dir / "chunked_files/Plots/results.xlsx"

for pix in heirloom.get_pixels(data_path, sheets=sheets):
    row, col = pix
    if not (row in pixel_dict.keys()):
        pixel_dict[row] = []
    pixel_dict[row].append(col)

i: int = 0
for row, cols in pixel_dict.items():
    data = sr["DC", row, cols]
    for ix, col in enumerate(cols):
        gap_size, tip_shape = heirloom.get_elec_geom(
            config["chip_map"], row, col
        )
        p = Pixel()
        p.data = data[0, ix, :]
        p.row = row
        p.col = col
        p.idx = i
        p.gap_size = gap_size
        p.tip_shape = tip_shape
        exp._pixel_list.append(p)
        i += 1

# Write H5 File
d_writer: dict = {"path": out_path}
writer: H5ExperimentWriter = H5ExperimentWriter()
writer.write_expr(exp, d_writer)
