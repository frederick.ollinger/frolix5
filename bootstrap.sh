#!/usr/bin/env sh

dos2unix -r * 
python3 -m pip install .
python3 -m pip install -r requirements.txt 
cd rbtdata &&  python3 -m pip install .
