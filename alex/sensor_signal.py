import numpy as np
from scipy import signal as sc_signal
from libCore.dsp.acf_psd import autocorr_FFT


def has_base_incorporation(
    data,
    fs=1000,
    error_max=0.005,
    t1=0.1,
    t2=2.5,  # Automated?
    slope_min=0.035,
    slope_max=0.065,
    decimate=10,
):
    """
    Return True if the data shows the signature of a DNA nucleotide base 
    incorporation, else return False.  The initial slope of the data  
    autocorrelation (ACF) provides the basis on which to discriminate. 
    
    Calculate the ACF and perform a linear fit between t1 and t2. Compare
    the slope and fit residual to thresholds in order to determine whether 
    the data indicates that a base incorporation has occurred or not. 
    
         slope_max > slope > slope_min and   fit error < error_max
         
    Parameters
    ----------
    data : array
        An array of at least 2*t2*acf_len pixel sensor electrical current 
        data points.
        
    fs : float
        Sample rate (Hz). Default = 1000.
        
    error_max : float
        Error threshold for ACF linear fit. Default = 0.005.
        
    t1 : float
        Seconds offset for start of linear fit of ACF
        
    t2 : float
        Seconds offset for stop of linear fit of ACF
        
    slope_min : float
        Min slope threshold for ACF linear fit. Default = 0.035.
       
    slope_max : float
        Max slope threshold for ACF linear fit. Default = 0.065
        
    decimate : int
        Factor by which to reduce data points before linear fit.
        
    Returns:
    --------
    found : bool  
        True if if the chemical signature was found, else False.
        
    slope : float 
        Estimated slope of the initial portion of the ACF
        
    res : float 
        Residual error from the linear fit of the initial portion of the ACF.
    """

    # Do the ACF
    fit_len = int((t2 - t1) * fs)
    auto_corr = autocorr_FFT(data)
    assert len(auto_corr) >= int(
        t2 * fit_len
    ), "Not enough data points in the autocorrelation"
    auto_corr = np.log(auto_corr[0 : int(t2 * fit_len)])  # Select

    # Decimate so that the linear fit is faster.
    auto_corr = sc_signal.decimate(np.nan_to_num(auto_corr), decimate)
    auto_corr = np.nan_to_num(auto_corr)

    # Account for decimation.
    fs = fs / decimate
    fit_len = fit_len // decimate

    # Perform linear fit.
    time = np.linspace(0, (t2 * fit_len - 1) / fs, num=int(t2 * fit_len))
    coef, res, _, _, _ = np.polyfit(
        time[int(t1 * fs) : int(t2 * fs)],
        auto_corr[int(t1 * fs) : int(t2 * fs)],
        1,
        full=True,
    )
    slope = abs(coef[0])
    res = res[0]

    # Do classification (chemical signal found or not found).
    if res < error_max and slope_max > slope > slope_min:
        found = True
    else:
        found = False

    return found, slope, res


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    import libCore.dsp.signal.SignalRandom as sigRand

    running_avg_filt_len = 100

    # Pink noise
    sigGauss = sigRand.Gaussian(
        mu=20.0, sigma=5.0, b=np.ones(running_avg_filt_len)
    )

    # Throw away pink noise filter transient response.
    sigGauss.Value(running_avg_filt_len)

    sigBrown = sigRand.Brownian()

    count_brown = 0
    count_pink = 0
    slope_avg_brown = 0
    slope_avg_pink = 0
    residual_avg_brown = 0
    residual_avg_pink = 0

    max_iters = 20
    iters = 0
    decimate = 10

    while iters < max_iters:

        # Test with a Brownian motion simulated signal.
        # Return should be 'False'.
        data = sigBrown.Value(100000).flatten()
        found, slope, res = has_base_incorporation(data, decimate=decimate)
        residual_avg_brown += res
        slope_avg_brown += slope
        if found:
            count_brown += 1

        # Test with a pink noise simulated signal.
        # Return should be 'False'.
        data = sigGauss.Value(100000).flatten()
        found, slope, res = has_base_incorporation(data, decimate=decimate)
        residual_avg_pink += res
        slope_avg_pink += slope
        if found:
            count_pink += 1

        iters += 1

    print("Brown % false positives: ", 100 * count_brown / max_iters)
    print("            avg slope =  ", slope_avg_brown / max_iters)
    print("         avg residual =  ", residual_avg_brown / max_iters)
    print("Pink  % false positives: ", 100 * count_pink / max_iters)
    print("            avg slope =  ", slope_avg_pink / max_iters)
    print("         avg residual =  ", residual_avg_pink / max_iters)

#     plt.figure()
#     plt.plot(range(len(data)), data, 'r--', label='raw')
#     plt.title('Brownian motion')
#     plt.legend()
#
#     plt.figure()
#     plt.plot(range(len(data)), data, 'r--', label='raw')
#     plt.title('Pink Noise')
#     plt.legend()
#     plt.show()
