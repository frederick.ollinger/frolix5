import numpy as np


def autocorr_FFT(x):
    """
    Return the autocorrelation function (ACF) of x. To compute it efficiently,  
    use the Wiener-Khinchin theorem (the Fourier transform of the ACF is the 
    power spectrum, and vice versa).
    
    Parameters
    ----------
    x : array
        Array of data.
        
    Parameters
    ----------
    C : array
        The positive portion of the ACF. 
    """
    psd = np.fft.fft(x) * np.conj(np.fft.fft(x)) / len(x)
    C = np.fft.ifft(psd).real
    C = C[C.size // 2 :]
    max_C = max(C)
    min_C = min(C)
    C = [(float(i) - max_C) / (max_C - min_C) + 1 for i in C]
    C = C[::-1]
    return C
