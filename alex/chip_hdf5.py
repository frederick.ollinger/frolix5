"""
Utilities for pixel data stored in HDF5 file format.
"""
import os
import natsort
import numpy as np
import cupy as cp
import matplotlib.pyplot as plt
from weighted_levenshtein import lev

import libCore.biochem.dna as dna
from libCore.device.bioSensor.sensor_signal import Roi, Signatures
from libCore.device.bioSensor.sequencer.base_call import (
    base_call_with_roi_shapelets,
)
from libCore.util.FilesFindAll import FilesFindAll
from libCore.util.Plots import ROYGBIV_colors
import h5py


class ChipData:

    #  {base : (window, , weight_additive, weight_multiplicative)}
    base_config = {
        "A": (60, 0, 1),
        "T": (60, 0, 1),
        "C": (60, 0.0, 1),
        "G": (60, -0.25, 1),
    }

    base_call_config = {
        "std_scale_hi": 0.2,  # Threshold high =  mean + std_scale_hi*std
        "std_scale_lo": 0.9,  # Threshold low  =  mean - std_scale_lo*std
        "base_config": base_config,
    }

    #     count_config = {'std_scale_hi' : 0.25,   # Threshold high =  mean + std_scale_hi*std
    #                     'std_scale_lo' : 1.20, # Threshold low  =  mean - std_scale_lo*std
    #                     'window'       : 80}
    #     count_config = {'std_scale_hi' : 0.25,   # Threshold high =  mean + std_scale_hi*std
    #                     'std_scale_lo' : 1.60, # Threshold low  =  mean - std_scale_lo*std
    #                     'window'       : 50}
    count_config = {
        "std_scale_hi": 0.25,  # Threshold high =  mean + std_scale_hi*std
        "std_scale_lo": 1.50,  # Threshold low  =  mean - std_scale_lo*std
        "window": 60,
    }

    STANDARD = 0  # SI team version of the Wizyme
    BIOSENSOR = 1  # Biosensor research version of the Wizyme
    CFX = 2  # Carl Fuller extension version of the senor

    config_BS = {
        Roi.T1: 0.5,
        Roi.T2: 3.5,
        Roi.ERR_MAX: 0.5,
        Roi.SLOPE_MIN: 0.05,
        Roi.SLOPE_MAX: 1.0,
        Roi.FS: 1000,
        Roi.dTr: 0.015,
        Roi.w_size: 20,
        Roi.step_size: 8,
    }

    def __init__(self, chip_id, sensor_type=BIOSENSOR, fs=1000, acf=False):

        root = r"C:\eng\data\H5"
        if not os.path.exists(root):
            root = r"\\fileserver\REMSLOCAL\Users\signal_processing\machine_learning\H5"

        print(root)

        self.chip_id = chip_id

        self.h5 = self.get_file(chip_id, root=root, acf=acf)

        if self.h5 is None:
            print("ChipData: Warning HDF5 file not found.")

        """Initialize the ROI for the sensor configuration"""
        if sensor_type == ChipData.STANDARD:
            self.roi = Roi(fs=fs, cfg=Roi.config_standard, useCuda=True)
        elif sensor_type == ChipData.BIOSENSOR:
            self.roi = Roi(fs=fs, cfg=Roi.config_biosensor, useCuda=True)
        elif sensor_type == ChipData.CFX:
            self.roi = Roi(fs=fs, cfg=Roi.config_cfx, useCuda=True)
        else:
            print("!!!Warning!!!! Sensor type is unknown. Init not done.")

    def analyze_base_incorporations(
        self,
        pixel,
        sequence,
        shapelet_config,
        roi_adjust,
        base_call_config=base_call_config,
        count_config=count_config,
        acf=False,
    ):

        h5, rois = self.find_rois_and_count_incorporations(
            pixel,
            # Expected base incorporation sequence. Template is the complement.
            sequence=sequence,
            do_plot_roi=False,  # Plot the ROI detection.
            # Start and stop sample number adjustment for each ROI.
            roi_adjust=roi_adjust,
            count_config=count_config,
            acf=acf,
        )

        title = (
            "G3-"
            + h5["exp_info"]["chip"][()]
            + ", ("
            + str(pixel[0])
            + ",  "
            + str(pixel[1])
            + ")"
        )

        for data_roi in rois:

            sequence_est = base_call_with_roi_shapelets(
                data_roi,
                shapelet_config,
                base_call_config=base_call_config,
                do_plot=True,
                title=title,
            )

            sequence_true = dna.Sequence(sequence)
            sequence_est = dna.Sequence("".join(sequence_est))

            # state width - G < T < A < C?
            print("True sequence =", str(sequence_true))
            print("Est. sequence =", str(sequence_est))
            print(
                "True Length = ",
                len(sequence_true),
                " Est. = ",
                len(sequence_est),
            )
            print(
                "True A cnt  = ",
                sequence_true.num_A(),
                " Est. = ",
                sequence_est.num_A(),
            )
            print(
                "True C cnt  = ",
                sequence_true.num_C(),
                " Est. = ",
                sequence_est.num_C(),
            )
            print(
                "True G cnt  = ",
                sequence_true.num_G(),
                " Est. = ",
                sequence_est.num_G(),
            )
            print(
                "True T cnt  = ",
                sequence_true.num_T(),
                " Est. = ",
                sequence_est.num_T(),
            )

            print(
                "Percent correct =",
                sequence_true.base_diff_error(sequence_est),
            )

            distance = lev("".join(sequence_est), sequence)
            print("Edit distance error =", distance)

        return h5, rois

    def find_rois_and_count_incorporations(
        self,
        pixel=None,  # None or tuple - (row, col)
        pixel_idx=0,  # If pixel is None, use this pixel number
        # Expected base incorporation sequence. Template is the complement.
        sequence="NNNN",  # N is unknown nucleotide.
        # Plot the ROI detection.
        do_plot_roi=False,
        # Adjust the start and stop sample of an ROI after extraction.
        # There may be more than one.
        roi_adjust=[(0, -1), (0, -1), (0, -1), (0, -1), (0, -1)],
        # left_boundary + roi_refine(0), right_boundary + roi_refine(1)
        roi_refine=(0, 0),
        count_config=count_config,
        acf=False,
    ):
        """
        Look for HDF5 data file with chip id (chip number in the same) and pixel. If 
        found, open and scan for regions of interest (ROI's) and count incorporations
        """
        if pixel == None or pixel == []:
            pixels = self.get_pixel_list(self.h5)
            print("Number of pixels = ", len(pixels))
            pixel = pixels[pixel_idx]

        row = pixel[0]
        col = pixel[1]

        title = (
            "G3-"
            + self.h5["exp_info"]["chip"][()]
            + ", ("
            + str(row)
            + ",  "
            + str(col)
            + ")"
        )

        _, data_full, _ = self.get_pixel(row, col)

        # Find ROI's
        # =========================================================================
        roi_indices, roi_segments = self.find_rois(
            row,
            col,
            do_plot=do_plot_roi,
            do_whole=False,
            roi_refine=roi_refine,
        )

        if roi_segments is None or len(roi_segments) < 1:
            return None, None

        # Analyze ROI for incorporation events
        # =========================================================================
        # Specify the matrix profle analysis time segment lengths.
        rois = []
        for i, roi_data in enumerate(roi_segments):
            data = np.array(
                roi_data[0][roi_adjust[i][0] : roi_adjust[i][1]]
            ).astype(np.float)
            rois.append(data)
            signatures = Signatures(sequence)

            # Matrix profile analysis.
            # ====================================================================
            sigs, sigs_match = signatures.find(
                roi=data,
                window=count_config["window"],
                std_scale_low=count_config["std_scale_lo"],
                std_scale_high=count_config["std_scale_hi"],
            )

            signatures.plot(
                roi=data,
                window=count_config["window"],
                signatures=sigs,
                signatures_match=sigs_match,
                title=title,
            )

            sequence_true = dna.Sequence(sequence)

            # state width - G < T < A < C?
            print("True sequence =", str(sequence_true))
            print("True Length = ", len(sequence_true))
            print("True A cnt  = ", sequence_true.num_A())
            print("True C cnt  = ", sequence_true.num_C())
            print("True G cnt  = ", sequence_true.num_G())
            print("True T cnt  = ", sequence_true.num_T())

        return self.h5, rois

    def find_rois(
        self,
        row,
        col,
        idx_phase=-1,
        do_plot=True,
        do_whole=False,
        width=None,
        roi_refine=(0, 0),
    ):

        if do_whole:
            pixel, data, _ = self.get_pixel(row, col)
        else:
            # Get the start and stop sample numbers for each phase.
            phases = self.get_phase_start_stop()

            # Get the pixel data.
            pixel, data, _ = self.get_pixel(
                row, col, phases[idx_phase][0], phases[idx_phase][1]
            )

        # Scan the pixel for an ROI.
        print("ACF assessment:")

        if self.pixel_qualify(data, threshold=25, peaking_treshold=-2):
            x_window, x_slope, bool_val = self.roi.find(data, 0, len(data))
            if do_plot and x_window is not None:
                self.roi_plot(pixel, data, x_slope, bool_val, x_window)
            elif do_plot:
                print("No ROI plot because ROI not found")
        else:
            print("pixel does not qualify [", row, col, "]")
            return None, None

        if x_window is None:
            print("Warning! No ROI's found. pixel = [", row, col, "]")
            return None, None

        roi_indices = self.roi.indices(
            data,
            x_window,
            bool_val,
            start_adjust=roi_refine[0],
            stop_adjust=roi_refine[1],
            width=width,
        )

        roi_segments = self.roi.segments(data, roi_indices)

        if len(roi_segments) == 0:
            print("Warning! No ROI's found. pixel = [", row, col, "]")

        return roi_indices, roi_segments

    def has_base_incorporation(self, data, start, stop):
        found, slope, res, svs = self.roi.has_signature(
            data[start:stop], decimate=10
        )
        if svs[0] / svs[1] > 10000:
            print(
                "Warning solution matrix was ill conditioned.||A^-1||/||A||=",
                svs[0] / svs[1],
            )
        return found, slope, res

    def pixel_qualify(self, data, threshold=25, peaking_treshold=-2):
        data_gpu = cp.asarray(data)
        return self.roi.qualify(data_gpu, threshold, peaking_treshold)

    def roi_plot(
        self,
        pixel,
        data,
        x_slope,
        bool_val,
        x_window,
        save_dir=None,
        fs=1000,
        w_size=20,
        step_size=10,
    ):

        title = (
            str(pixel)
            + "ROI detection , W-Size: "
            + str(w_size)
            + " s, "
            + "Step-Size: "
            + str(step_size)
            + " s"
        )
        self.roi.plot(
            data, x_slope, bool_val, x_window, save_dir=None, title=title
        )

    def get_file(
        self,
        chip_id,
        root=r"\\fileserver\REMSLOCAL\Users\signal_processing\machine_learning\H5",
        acf=False,
    ):
        """
        Look for HDF5 data file with chip id (chip number in the same). If 
        found, open and return the H5 file object.
        """
        if acf:
            pattern = "G3A-{}*_acf.h5".format(chip_id)
        else:
            pattern = "G3A-{}*.h5".format(chip_id)
        for path in FilesFindAll(root, pattern):
            return h5py.File(path, mode="r")

        print("!!!! Chip ", chip_id, "experiment H5 data file not found!!!!")
        return None

    def get_phase_start_stop(self):
        """Return the start and stop sample values for all non-IGNORE phases"""
        phase_start_stop = []
        keys = natsort.natsorted(
            list(self.h5["exp_info"]["phase_info"].keys())
        )
        for key in keys:
            phase = self.h5["exp_info"]["phase_info"][key]
            if phase["phase_name"][()] != "IGNORE":
                phase_start_stop.append(
                    (
                        max(0, int(phase["t_start"][()])),
                        int(phase["t_stop"][()]),
                        phase["phase_name"][()],
                    )
                )
        return phase_start_stop

    def get_pixel_list(self):
        """Return a list of pixels via their (row,col) coordinates"""
        pixels = []
        for i in range(len(self.h5["pixel_info"])):
            pixel = "pixel[{}]".format(i)
            row = self.h5["pixel_info"][pixel]["row"][()]
            col = self.h5["pixel_info"][pixel]["col"][()]
            pixels = pixels + [(row, col, i)]
        return pixels

    def get_pixel(self, row, col, start_idx=0, stop_idx=-1):
        for i in range(len(self.h5["pixel_info"])):
            pixel = "pixel[{}]".format(i)
            if (
                row == self.h5["pixel_info"][pixel]["row"][()]
                and col == self.h5["pixel_info"][pixel]["col"][()]
            ):
                if stop_idx == -1:
                    stop_idx = len(self.h5["pixel_info"][pixel]["raw_signal"])
                data = self.h5["pixel_info"][pixel]["raw_signal"][
                    start_idx:stop_idx
                ]
                return pixel, data, i
        return None, None, None

    def get_pixel_id(self, row, col):
        for i in range(len(self.h5["pixel_info"])):
            pixel = "pixel[{}]".format(i)
            if (
                row == self.h5["pixel_info"][pixel]["row"][()]
                and col == self.h5["pixel_info"][pixel]["col"][()]
            ):
                return pixel, i
        return None, None

    def get_signal_from_phase(self, pixel, phase_ID):
        if isinstance(pixel, int):
            pixel = "pixel[{}]".format(pixel)
        signal = self.h5["pixel_info"][pixel]["raw_signal"][()]
        phases = natsort.natsorted(
            list(self.h5["exp_info"]["phase_info"].keys())
        )
        phase = phases[phase_ID]
        start_idx = int(
            (self.h5["exp_info"]["phase_info"][phase]["t_start"][()])
        )
        stop_idx = int(
            (self.h5["exp_info"]["phase_info"][phase]["t_stop"][()])
        )
        return signal[start_idx:stop_idx]

    def plot_pixel(self, pixel, phase_ID=-1, sig_aux=None, sig_aux_label=""):

        plt.figure()
        if isinstance(pixel, int):
            pixel = "pixel[{}]".format(pixel)
        row = self.h5["pixel_info"][pixel]["row"][()]
        col = self.h5["pixel_info"][pixel]["col"][()]
        phases = natsort.natsorted(
            list(self.h5["exp_info"]["phase_info"].keys())
        )

        if phase_ID == -1:
            y = self.h5["pixel_info"][pixel]["raw_signal"][()]
            i = 0
            for phase_ID, phase in enumerate(
                phases
            ):  # for phases after post-bridging
                start_idx = max(
                    int(
                        (
                            self.h5["exp_info"]["phase_info"][phase][
                                "t_start"
                            ][()]
                        )
                    ),
                    0,
                )
                stop_idx = int(
                    (self.h5["exp_info"]["phase_info"][phase]["t_stop"][()])
                )

                if stop_idx > len(y):
                    stop_idx = len(y)
                    print(
                        "Warning! Phase stop index value is beyond the length of the data."
                    )

                x = np.linspace(
                    start_idx, stop_idx, len(y[start_idx:stop_idx])
                )

                if (
                    self.h5["exp_info"]["phase_info"][phase]["phase_name"][()]
                    == "IGNORE"
                ):  # set color to black
                    plt.plot(x, y[start_idx:stop_idx], color="k")
                else:  # get color from ROYGBIV_colors dict
                    plt.plot(
                        x,
                        y[start_idx:stop_idx],
                        color=ROYGBIV_colors[(i + 1) % len(ROYGBIV_colors)],
                        label=str(phase_ID)
                        + ": "
                        + self.h5["exp_info"]["phase_info"][phase][
                            "phase_name"
                        ][()],
                    )
                    i += 1
            title = (
                "G3-"
                + self.h5["exp_info"]["chip"][()]
                + ", "
                + self.h5["exp_info"]["bridge"][()]
                + ", "
                + self.h5["exp_info"]["buffer"][()]
                + ", ("
                + str(row)
                + ",  "
                + str(col)
                + ")"
            )
        else:
            phase = phases[phase_ID]
            y = self.get_signal_from_phase(pixel, phase_ID)

            start_idx = int(
                (self.h5["exp_info"]["phase_info"][phase]["t_start"][()])
            )
            stop_idx = int(
                (self.h5["exp_info"]["phase_info"][phase]["t_stop"][()])
            )
            x = np.linspace(start_idx, stop_idx, len(y[start_idx:stop_idx]))
            plt.plot(x, y[start_idx:stop_idx], color="k")
            title = (
                "Chip:"
                + self.h5["exp_info"]["chip"][()]
                + ", bridge:"
                + self.h5["exp_info"]["bridge"][()]
                + ", buffer:"
                + self.h5["exp_info"]["buffer"][()]
                + ", phase:"
                + self.h5["exp_info"]["phase_info"][phase]["phase_name"][()]
                + ", row="
                + str(row)
                + ", col="
                + str(col)
            )

        if sig_aux is not None:
            stop_idx = min(stop_idx, len(sig_aux))
            sig_aux = sig_aux[start_idx:stop_idx]
            plt.plot(
                x[: len(sig_aux)],
                sig_aux,
                color=ROYGBIV_colors[(i + 1) % len(ROYGBIV_colors)],
                label=sig_aux_label,
            )

        plt.title(title)
        plt.grid()
        plt.xlabel("ms")
        plt.ylabel("Current (ADC counts)")
        plt.legend(loc="best")
        return y

    def plot_pixel_segment(self, pixel, start_idx, stop_idx):
        if isinstance(pixel, int):
            pixel = "pixel[{}]".format(pixel)
        y = self.h5["pixel_info"][pixel]["raw_signal"][()]
        x = np.linspace(start_idx, stop_idx, len(y[start_idx:stop_idx]))
        plt.plot(
            x,
            y[start_idx:stop_idx],
            color=ROYGBIV_colors[(1) % len(ROYGBIV_colors)],
            label="raw",
        )
        plt.title(self.h5["exp_info"])
        plt.xlabel("ms")
        plt.ylabel("Current (ADC counts)")
        plt.legend(loc="best")


if __name__ == "__main__":

    chip_id = 41608
    roi = (42, 208, 1109620, 1118000)
    sequence = "GCGAAAGCGAAAGCGAAAGCGAAAGCGAAAGCGAAAAA"

    first_A = 7208
    last_A = first_A + 2 * ChipData.base_config["A"][0]
    first_C = 235  # 2928
    last_C = first_C + 2 * ChipData.base_config["C"][0]
    first_G = 37  # 72
    last_G = first_G + 2 * ChipData.base_config["G"][0]
    shapelet_config = [
        (first_A, last_A, "A"),
        (first_C, last_C, "C"),  # (242,322, 'C'), # (352,432, 'C'),
        (first_G, last_G, "G"),
    ]

    roi_adjust = [(9615, -6830)]

    chip_data = ChipData(chip_id, sensor_type=ChipData.BIOSENSOR)

    #     print(chip_data.get_phase_start_stop())
    #
    #     pixel,_ = chip_data.get_pixel_id(roi[0], roi[1])
    #     plt.figure()
    #     chip_data.plot_pixel(pixel=pixel)

    chip_data.analyze_base_incorporations(
        pixel=roi,
        shapelet_config=shapelet_config,
        roi_adjust=roi_adjust,
        sequence=sequence,
    )

    plt.show()
