#!/usr/bin/env python3

# import os, sys

from rbtdata.expr.h5.experiment_reader import H5ExperimentReader
from rbtdata.expr.h5.experiment_writer import H5ExperimentWriter
from rbtdata.expr.expr import Experiment

reader: H5ExperimentReader = H5ExperimentReader()
d_reader: dict = {"path": "/data/data/G3A-1806.h5"}
exp: Experiment = reader.read_expr(d_reader)

d_writer: dict = {"path": "/data/data/G3A-1806-out.h5"}
writer: H5ExperimentWriter = H5ExperimentWriter()
writer.write_expr(exp, d_writer)
