from datetime import datetime
import glob
import os
from typing import Any, Generator, Dict, List

import pandas as pd
import libData.file.network_drives as nd


def dataset(excel_path: str) -> Generator[pd.Series, None, None]:
    """Load experiment data from Excel file and find the relevant directories.

    :param excel_path: Excel file location
    :return: data with added directory locations
    """

    df = pd.read_excel(excel_path)
    df["local_path"] = [search(row) for _, row in df.iterrows()]
    print(df.head())

    return df


def search(row: pd.Series) -> str:
    """Find 8CAB experiment directory from metadata.

    :param row: experiment metadata
    :return: experiment directory path
    """
    drive = nd.path("rems_data")

    date_stamp = row["date"].split("T")[0]
    date = datetime.strptime(date_stamp, r"%Y%m%d").date()
    day_path = os.path.normpath(
        "{0}/{0}-{1:02d}/{0}-{1:02d}-{2:02d}".format(
            date.year, date.month, date.day,
        )
    )

    name_glob = f"{row['date']}_8CAB-*_W{row['wafer']}*{row['chip']}"
    expr_glob = f"{drive}/Data 1/{day_path}/{name_glob}"  # os.path.join(drive, "/Data 1", day_path, name_glob)
    try:
        return os.path.normpath(next(glob.iglob(expr_glob)))
    except StopIteration:
        print(f"libData.file.excel: Unable to find glob matching {expr_glob}.")


if __name__ == "__main__":
    drive = nd.path("remslocal")
    folder = "Users/signal_processing/machine_learning"
    dataset(f"{drive}/{folder}/pulse_metrics_master.xlsx")
