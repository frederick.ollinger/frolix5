from collections import namedtuple
import numpy as np
import re
from typing import Iterator, Tuple
import unittest
from unittest.mock import patch
from libData.file.tdms import Board, ChunkElectrode, ChunkSignal


class MockBoard:
    """Mock of libData.file.tdms.Board."""

    def __init__(self):
        """Create new MockBoard instance."""

        self.dimensions = (4, 8)
        self.sample_rate = 1
        self.samples_per_chunk = 10
        self.time_per_chunk = 10.0

    def channel(self, electrode: Tuple[int, int]) -> Tuple[str, str]:
        """Mock group and channel fetcher.
        
        :param electrode: electrode location as (row, column)
        :return: (group, channel) location in TDMS file
        """

        return ("Frame Data", f"Row {electrode[0]}, Column {electrode[1]}")


class MockFileAPI:
    """Mock of libData.file.tdms.FileAPI."""

    def __init__(self):
        """Create new MockBoard instance."""

        pass

    def board(self) -> MockBoard:

        return MockBoard()

    def get_file(self, file_path: str) -> str:
        """Get file."""

        return file_path

    def glob_files(self, board: Board, path_id: str) -> Iterator[str]:

        return iter(["foo", "bar"])


class MockTdmsFile:
    """Mock of nptmds.TdmsFile."""

    def __init__(self, path):
        """Create new MockTdms instance."""

        self.path = path

    def object(self, group, channel):
        """Mock data fetcher for TDMS files.
        
        TODO: integrate usuage of group and channel arguments

        :param group:
        :param channel:
        :return:
        """

        obj = namedtuple("Obj", ["raw_data"])
        idx = int(re.match(r"^.*_R(\d+)_(\d+)_DC\.tdms", self.path).group(2))
        return obj(np.arange(10 * idx, 10 * (idx + 1)))


class TestBoard(unittest.TestCase):
    def test_channel_eight_cab(self):
        """Test correct channel response for 8CAB board."""

        board = Board.EIGHTCAB
        electrode = (0, 3)

        expected = ("8CAB_Active_Bridging", f"Electrode 4")
        actual = board.channel(electrode)
        self.assertEqual(actual, expected)

    def test_channel_gen_3(self):
        """Test correct channel response for Gen3 board."""

        board = Board.GEN3
        electrode = (12, 172)

        expected = ("Frame Data", f"Row 12, Column 172")
        actual = board.channel(electrode)
        self.assertEqual(actual, expected)


@patch("libData.file.tdms.TdmsFile", MockTdmsFile)
class TestChunkElectrode(unittest.TestCase):
    def setUp(self):

        mock_tdms_paths = [
            "mock_path_R0002_0000_DC.tdms",
            "mock_path_R0002_0001_DC.tdms",
            "mock_path_R0002_0002_DC.tdms",
            "mock_path_R0002_0003_DC.tdms",
        ]

        self.signal = ChunkElectrode(
            MockBoard(), MockFileAPI(), mock_tdms_paths, [2, 5], 2
        )

    def test_convert_slice(self):
        """Test converting slice to modulo file slice."""

        slc = slice(4, 11, 2)
        expected = (
            [
                "mock_path_R0002_0000_DC.tdms",
                "mock_path_R0002_0001_DC.tdms",
                "mock_path_R0002_0002_DC.tdms",
            ],
            slice(8, 2, 4),
        )
        actual = self.signal.convert_slice(slc)
        self.assertEqual(actual, expected)

    def test_find_index(self):
        """Test spliting index into file index and file modulo index."""

        expected = (1, 2)
        actual = self.signal.find_index(6)
        self.assertEqual(actual, expected)

    def test_step(self):

        expected = [8, 12, 16, 20]
        actual = self.signal[4:11:2].tolist()
        self.assertEqual(actual, expected)

    def test_one_file(self):

        expected = [0, 2, 4, 6]
        actual = self.signal[0:4].tolist()
        self.assertEqual(actual, expected)

    def test_two_files(self):

        expected = [6, 8, 10, 12]
        actual = self.signal[3:7].tolist()
        self.assertEqual(actual, expected)

    def test_three_files(self):

        expected = [8, 10, 12, 14, 16, 18, 20]
        actual = self.signal[4:11].tolist()
        self.assertEqual(actual, expected)

    def test_all(self):

        self.signal.downsample = 1
        expected = list(range(40))
        actual = self.signal[:].tolist()
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
