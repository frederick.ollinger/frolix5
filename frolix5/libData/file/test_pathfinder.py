"""Unit tests for pathfinder module."""


import datetime
import pathlib
import sys

import pytest

from libData.file import pathfinder


def test_dirpath(mocker) -> None:
    """Check that dirpath function logic performs as expected."""

    sys.platform = "linux"

    expected = pathlib.Path(
        "/mnt/gen3/data1/GEN3DATA1/Processed/2019/2019-10/2019-10-25/20191025T140743_GEN0-02_G3-743"
    )
    mocker.patch.object(pathlib.Path, "exists", lambda self: self == expected)

    finder = pathfinder.PathFinder("20191025T140743_GEN0-02_G3-743")
    actual = finder.dirpath()
    assert actual == expected


def test_parse_name() -> None:
    """Check for proper name and chip parsing from experiment directory names."""

    finder = pathfinder.PathFinder("20200102T155940_GEN3-02_G3-947")

    expected = datetime.date(2020, 1, 2), "G3-947"
    actual = finder.parse_name()
    assert actual == expected


if __name__ == "__main__":
    pytest.main()
