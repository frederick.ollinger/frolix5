"""
Experiment path finder.
principal author: macklan weinstein
refactored by: james mullinix
© Roswell Biotechnologies 2020
"""


class PathFinder:
    import pathlib
    from datetime import datetime
    from typing import Tuple

    def __init__(self, expr_dir: str):
        """ Class initializer.
            Performs all routines.
        :return: full experiment path
        """
        self.expr_dir = expr_dir

    def dirpath(self) -> pathlib.Path:
        """Find Gen3 experiment directory path from directory basename.

        On Windows and macOS, this function will return the network drive path. On Linux, this
        function will return the mount path, assuming that //fileserver2/GEN3DATA1 is mounted
        to /mnt/gen3/data1/GEN3DATA1 and //fileserver/REMS_DATA is mounted to /mnt/8cab.

        :return: None
        :raise FileNotFoundError: if full experiment path does not exists
        """
        import sys

        if sys.platform == "linux":
            paths = {
                "8CAB": self.pathlib.Path("/mnt/8cab/Data 1"),
                "Gen3Data1": self.pathlib.Path(
                    "/mnt/gen3/data1/GEN3DATA1/Processed"
                ),
                "Gen3Data2": self.pathlib.Path(
                    "/mnt/gen3/data2/GEN3DATA2/Processed"
                ),
                "Gen3Data3": self.pathlib.Path(
                    "/mnt/gen3/data3/GEN3DATA3/Processed"
                ),
            }
        else:
            paths = {
                "8CAB": self.pathlib.Path("//fileserver/REMS_DATA/Data 1"),
                "Gen3Data1": self.pathlib.Path(
                    "//fileserver2/GEN3DATA1/GEN3DATA1/Processed"
                ),
                "Gen3Data2": self.pathlib.Path(
                    "//fileserver2/GEN3DATA2/GEN3DATA2/Processed"
                ),
                "Gen3Data3": self.pathlib.Path(
                    "//fileserver2/GEN3DATA3/GEN3DATA3/Processed"
                ),
            }

        date, _ = self.parse_name()
        day_path = self.pathlib.Path(
            "{0}/{0}-{1:02d}/{0}-{1:02d}-{2:02d}".format(
                date.year, date.month, date.day,
            )
        )

        # Easier to try all generation options, than to parse generation from folder name
        for gen, path in paths.items():
            self.expr_path = path / day_path / self.expr_dir
            if self.expr_path.exists():
                break
        else:
            raise FileNotFoundError(
                f"experiment {self.expr_dir} does not exist"
            )

        return self.expr_path

    def parse_name(self) -> Tuple[datetime, str]:
        """Parse experiment directory basename for date and chip.
        :return: date, chip
        """

        date_stamp = self.expr_dir.split("T")[0]

        # Fault early if folder does not have the proper timestamp
        try:
            date = self.datetime.strptime(date_stamp, r"%Y%m%d").date()
        except ValueError as xcpt:
            raise FileNotFoundError(
                f"folder {self.expr_dir} does not follow %Y%m%d timestamp format"
            )

        chip = self.expr_dir.split("_")[-1]
        return date, chip
