import os, pathlib, pickle, progressbar
import numpy as np
import pandas as pd
from typing import Optional, List, Dict, Tuple

from libCore.device.bioSensor.sequencer.main import Config, Sequencer

from rbtdata.expr import event, search
from libData.file.excel import dataset
from libData.file.tdms import ChunkSignal
import libData.file.network_drives as nd


class Dataset:
    """ Purpose: to append signal information to machine learning datasets  

    arguments
    --------------------
    dataset_name: str
        name of an excel dataset in the machine_learning folder.

    returns
    --------------------
    exprs: dataframe
        dataframe version of the original excel shee
    dataset_name_phases.pkl: pickled dict
        contains all experiment information for each experiment as well
        as raw phase signal with phase information, packaged as a list
    dataset_name_rois.pkl: pickled dict
        contains all experiment information for each roi as well as raw 
        signal from roi. can easily be transferred to dataframe
    """

    def __init__(self, dataset_name: str):
        drive = nd.path("remslocal")
        self.dataset_name = dataset_name
        self.location = f"{drive}/Users/signal_processing/machine_learning"
        self.exprs = dataset(f"{self.location}/{dataset_name}")

        self.roi_keys = [
            "date",
            "chip",
            "wafer",
            "electrode",
            "enzyme",
            "template",
            "nucleotide",
            "roi",
            "detrended",
            "baseline",
            "label",
        ]
        self.phase_keys = [
            "date",
            "chip",
            "wafer",
            "electrode",
            "enzyme",
            "template",
            "nucleotide",
            "phase",
        ]

    def __call__(
        self,
        write_file: bool = True,
        do_detrend: bool = False,
        collect_phases: bool = True,
        collect_rois: bool = False,
        detrend_cfg=Config(),
    ) -> List[Dict]:
        """ generate datasets when called. 
        
        arguments 
        --------------------
        write_file: bool
            choose to write datasets to file
        do_detrend: bool
            choose to collect detrended and baseline signal
        collect_phases: bool
            choose to generate whole phase data
        collect_rois: bool
            choose to generate roi data
        detrend_cfg: Config
            supply c++ pipeline configuration
        """

        self.outputs = []
        self.do_detrend = do_detrend
        self.detrend_cfg = detrend_cfg
        self.write_file = write_file

        if collect_phases:
            # group by local path and electrode since phases are on an
            # electrode scale.
            electrodes = self.exprs.groupby(["local_path", "electrode"])

            self.phase_dict = {key: [] for key in self.phase_keys}
            self._generate_dataset(
                data_dict=self.phase_dict,
                data_name="phases",
                group=electrodes,
                method=self._append_phases,
            )

        if collect_rois:
            # group down to t_start and t_stop since those define rois;
            # multiple rois could be from the same electrode
            rois = self.exprs.groupby(
                ["local_path", "electrode", "t_start", "t_stop"]
            )

            self.roi_dict = {key: [] for key in self.roi_keys}
            self._generate_dataset(
                data_dict=self.roi_dict,
                data_name="rois",
                group=rois,
                method=self._append_roi,
            )

        return self.outputs

    def _generate_dataset(self, data_dict, data_name, group, method) -> None:
        """ generates dataset in the call."""
        self._get_metadata(data_dict, group)
        self._get_signal(group, method)
        self._save(data_dict, data_name)
        self.outputs.append(data_dict)

    def _get_metadata(self, data_dict, group) -> None:
        """ grab experiment info for each new entry into data_dict. 
        
        arguments
        --------------------
        data: dict
            the dict where raw signals will be stored.
        group: dataframe/dataframe group
            this is the iterator for generating raw signal data.
        """
        for _, row in group.head(1).iterrows():
            data_dict["date"].append(row.date)
            data_dict["chip"].append(row.chip)
            data_dict["wafer"].append(row.wafer)
            data_dict["electrode"].append(row.electrode)
            data_dict["enzyme"].append(row.enzyme)
            data_dict["template"].append(row.template)
            data_dict["nucleotide"].append(row.nucleotide)

    def _get_signal(self, group, method) -> None:
        """ extract and store raw signals. """

        bar = progressbar.ProgressBar(max_value=len(group))
        idx = 0

        for attrs, df in group:
            method(attrs, df)
            bar.update(idx)
            idx += 1

    def _append_phases(self, attrs, df) -> None:
        """ method to phase information and extract raw phase signal to phase dataset. """

        # find the log file and use event library to get the phases
        log = search.event_log(pathlib.Path(attrs[0]))
        phases = event.phases(log)
        phase_list = []

        for phase in phases:

            # we only want sequencing phases
            # some incorrectly labeled phases have negative start times
            if phase.phase == "SEQUENCING" and phase.start > 0:
                signal = ChunkSignal(attrs[0])[int(attrs[1]) - 1][
                    int(phase.start * 1000) : int(phase.end * 1000)
                ]
                detrended, baseline = self._detrend(signal)
                phase_list.append(
                    [phase.phase, phase.message, signal, detrended, baseline,]
                )

        self.phase_dict["phase"].append(phase_list)

    def _append_roi(self, attrs, df) -> None:
        """ method to append raw roi signal to roi dataset. """
        length = int((attrs[3] - attrs[2]) * 1000)  # roi length
        signal = ChunkSignal(attrs[0])[int(attrs[1]) - 1][
            int(attrs[2] * 1000) : int(attrs[3] * 1000)
        ]
        detrended, baseline = self._detrend(signal)
        label = self._get_label(df, length)

        self.roi_dict["roi"].append(signal)
        self.roi_dict["detrended"].append(detrended)
        self.roi_dict["baseline"].append(baseline)
        self.roi_dict["label"].append(label)

    def _get_label(self, df, length) -> np.ndarray:
        """ create a label at every point a pulse is id'ed in an roi. """
        label = np.zeros((4, length))
        for _, row in df.iterrows():
            type_idx = self._nuc2num(row.nucleotide)
            start_idx = row.start_idx - int(row.t_start * 1000)
            stop_idx = row.stop_idx - int(row.t_stop * 1000)
            label[type_idx, start_idx:stop_idx] = 1
        return label

    def _nuc2num(self, string: str) -> int:
        if "A" in string.upper():
            return 0
        elif "C" in string.upper():
            return 1
        elif "G" in string.upper():
            return 2
        elif "T" in string.upper():
            return 3
        else:
            return 4

    def _detrend(self, sig_in) -> Tuple[Optional[np.ndarray]]:
        """ generate detrended and baseline signal using c++ pipeline
        Note: requires boost python 1.71.0
        """
        sig_out = None
        sig_out_baseline = None

        if self.do_detrend:

            sig_out = np.zeros(len(sig_in))
            sig_out_baseline = np.zeros(len(sig_in))

            detrender = Sequencer(
                logger=None,
                roi_finder=None,
                clf_algo=None,
                cfg=self.detrend_cfg,
                debug=False,
            )
            detrender.Condition2(sig_out, sig_out_baseline, sig_in)

        return sig_out, sig_out_baseline

    def _save(self, data, data_name) -> None:
        """ method to save any dict generated. 
        the save location will always be in the machine_learning folder
        to keep ml datasets in a central location accessible to all users.
        
        arguments
        --------------------
        data: dict
            object to save
        data_name: str
            type of data saved; rois, phases,...
        """
        if self.write_file:
            home = os.path.expanduser("~")
            expr_name = self.dataset_name.split(".")
            file_path = f"{self.location}/{expr_name[0]}_{data_name}.pkl"
            file = open(file_path, "wb")
            pickle.dump(data, file)
            file.close()
            print(f"{file_path} successfully written.")


def main() -> None:
    """ to be run by the if __name__ == "__main__", packaged here to
    be more agreeable by some utilities run by some Roswell users.
    """
    # provide choice of dataset...
    ds_name = input(
        "Enter the name of the dataset [default: BREX__01222020_A30_dT13dG08__C30_dG08dT13.xlsx]: "
    )  # on linux box: "/mnt/remslocal/Users/zach/MATLAB/pulse_metrics_master.xlsx"

    # ... but fall back to a default
    if ds_name == "":
        ds_name = "BREX__01222020_A30_dT13dG08__C30_dG08dT13.xlsx"

    default_cfg = Config()
    dset = Dataset(ds_name)

    do_detrend = bool(
        input(
            "Enter anything to perform detrending or nothing to skip."
            + "Note: your system must have boost python installed and binaries compiled.\n"
        )
    )
    write_file = bool(
        input("Enter anything to write to file or nothing to skip.\n")
    )
    df = dset(
        write_file=write_file,
        do_detrend=do_detrend,
        collect_phases=True,
        collect_rois=True,
        detrend_cfg=default_cfg,
    )  # perform all tasks


if __name__ == "__main__":
    main()
