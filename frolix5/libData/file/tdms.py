from enum import Enum
from glob import iglob
from nptdms import TdmsFile
import numpy as np
import os
import re
from typing import (
    Dict,
    Iterable,
    Iterator,
    List,
    Optional,
    Sequence,
    Tuple,
    Union,
)

# from frolix5.libData.file.aws import REMSUpload


class Board(Enum):
    """Class for decoupling board specific details from ChunkSignal and ChunkEletrode."""

    EIGHTCAB = 1
    GEN3 = 3

    def __init__(self, gen: int):
        """Create new Board instance.

        :param gen: board generation
        """

        data = {
            1: {
                "dimensions": (8, 1),
                "filename_regex": re.compile(r"^.*_R(\d+)_(\d+)_DC\.tdms$"),
                "sample_rate": 20_000,
                "samples_per_chunk": 3_000_000,
                "time_per_chunk": 150.0,
            },
            3: {
                "dimensions": (64, 256),
                "filename_regex": re.compile(r"^.*_R(\d+)_(\d+)_DC\.tdms$"),
                "sample_rate": 1_000,
                "samples_per_chunk": 600_000,
                "time_per_chunk": 600.0,
            },
        }

        for name, attr in data[gen].items():
            setattr(self, name, attr)

    def channel(self, electrode: Tuple[int, int]) -> Tuple[str, str]:
        """Return group and channel TDMS names for accessing raw data.
        
        :param electrode: electrode location as (row, column)
        :return: (group, channel) location in TDMS file
        """

        if self == self.EIGHTCAB:
            return ("8CAB_Active_Bridging", f"Electrode {electrode[1] + 1}")
        else:
            return ("Frame Data", f"Row {electrode[0]}, Column {electrode[1]}")


class ChunkElectrode:
    """Lazy signal reader for eletrode data spread across several files."""

    def __init__(
        self,
        board: Board,
        file_api: "FileAPI",
        tdms_paths: Dict[int, str],
        electrode: Tuple[int, int],
        downsample: int,
    ):
        """Create new ChunkElectrode instance.
 
        Expected to only be created by ChunkSignal.

        :param board: Board object
        :param file_api: file api for searching filenames on a system
        :param tdms_paths: integer enumerated dictionary instead of list in case
                           of missing chunk files
        :param electrode: electrode location as (row, column)
        :param downsample: skip every (downsample -1) sample
        """

        self.board = board
        self.file_api = file_api
        self.downsample = downsample
        self.electrode = electrode
        self.tdms_paths = tdms_paths

    def __getitem__(self, key: Union[int, slice]) -> Union[float, np.array]:
        """Get item or items from TMDS file.

        :param key: integer or slice index
        :return: values from TDMS file
        """

        if isinstance(key, int):
            slc = slice(key, key + 1, 1)
            return self.get_elems(slc)
        elif isinstance(key, slice):
            return self.get_elems(key)
        else:
            raise TypeError(
                f"ChunkSignal indices must be integers or slices, not {type(key)}"
            )

    def __len__(self) -> int:
        """Return full electrode signal length.

        :return: signal length
        """

        init_length = self.board.samples_per_chunk * (len(self.tdms_paths) - 1)
        return init_length + self.last_file_length()

    def convert_slice(self, key: slice) -> Tuple[List[str], slice]:
        """Grabs files for slice and convert slice to modulo multiple file format.

        The slice is converted to (start_mod_idx, stop_mod_idx, step), where 
        start_mod_idx is the starting index in the first TDMS chunk file and 
        stop_mod_idx is one past the final idx in the final TDMS chunk file. Note that 
        start_mod_idx, stop_mod_idx, and step will always be nonnegative integers.
        
        :param key: general signal slice
        :return: (TDMS files, slice inside array from TMDS file)
        """

        if key.start is None:
            start_file_idx, start_mod_idx = 0, 0
        else:
            start_file_idx, start_mod_idx = self.find_index(key.start)

        if key.stop is None:
            stop_file_idx, stop_mod_idx = (
                len(self.tdms_paths),
                self.last_file_length(),
            )
        elif key.stop < 0:
            stop = self.last_file_length() + key.stop
            stop_file_idx, stop_mod_idx = self.find_index(stop)
        else:
            stop_file_idx, stop_mod_idx = self.find_index(key.stop)

        step = self.downsample * key.step if key.step else self.downsample
        files = self.tdms_paths[start_file_idx : stop_file_idx + 1]
        return files, slice(start_mod_idx, stop_mod_idx, step)

    def csv_address(self, csv_path) -> Dict[str, Union[int, str]]:
        """Get TDMS file binary info from CSV file.

        :params csv_path: path to CSV file
        :return: address, length, type, and size values
        """

        channel = "/'{}'/'{}'".format(*self.board.channel(self.electrode))
        keys = ("address", "length", "type", "size")
        intos = (int, int, str, int)

        rel_path = self.file_api.get_file(csv_path)
        with open(rel_path, "r") as handle:
            for line in handle:
                strings = line.strip().split(";")
                if strings[0] == channel:
                    vals = (
                        into(string)
                        for into, string in zip(intos, strings[1:])
                    )
                    return dict(zip(keys, vals))

    def find_index(self, idx: int) -> Tuple[int, int]:
        """Convert index into backend index pair for data in chunked files.
        
        :param idx: general signal index
        :return: (TDMS file list index, index inside array from TMDS file)
        """

        idx, chunk_size = self.downsample * idx, self.board.samples_per_chunk
        return idx // chunk_size, idx % chunk_size

    def get_elems(self, key: slice) -> np.array:
        """Retrieve signal slice from possibly multiple files
        
        :param key: general signal index slice
        :return: signal values at index slice locations
        """

        files, idxs = self.convert_slice(key)
        chunk_size = self.board.samples_per_chunk

        signals, n = [], len(files)
        for k, tdms_file in enumerate(files):
            if k == 0:
                start = idxs.start
                # support for case where n = k
                stop = idxs.stop if len(files) == 1 else chunk_size
            elif k == n - 1:
                start, stop = (chunk_size - start) % idxs.step, idxs.stop
            else:
                start, stop = (chunk_size - start) % idxs.step, chunk_size

            file_idxs = slice(start, stop, idxs.step)
            signal = self.read_data(tdms_file, file_idxs)
            signals.append(signal)

        return np.concatenate(signals)

    def get_timeslice(self, start: float, stop: float) -> np.array:
        """Retrieve signal slice from using bounds based on seconds from start.

        :param start: array index in seconds from experiment start 
        :param stop: array index in seconds from experiment start 
        :return: array values
        """

        slc = slice(
            round(start * self.board.sample_rate),
            round(stop * self.board.sample_rate),
        )
        return self[slc]

    def last_file_length(self) -> int:
        """Find length of array in last TDMS chunk file.
        
        :return: length of array in last TDMS chunk file
        """

        tdms_path = self.tdms_paths[-1]
        tdms_name, _ = os.path.splitext(tdms_path)
        csv_path = f"{tdms_name}_RawDataAddresses.csv"

        if os.path.isfile(csv_path):
            info = self.csv_address(csv_path)
            return info["length"]
        else:
            return len(self.read_nptmds(tdms_path, slice(0, None, 1)))

    def read_binary(self, csv_path, tdms_path, slc):
        """Read array from TDMS file using binary handle.
        
        :param tdms_path: TDMS file path for reading
        :param slc: array index slice
        :return: array values from TDMS file
        """

        info = self.csv_address(csv_path)
        start = info["address"] + info["size"] * slc.start
        stop = info["address"] + info["size"] * slc.stop

        with open(tdms_path, "rb") as handle:
            handle.seek(start, 0)
            byte_array = handle.read(info["size"] * (stop - start))

        return np.frombuffer(byte_array, dtype=np.uint8)[
            0 : slc.stop : slc.step
        ]

    def read_data(self, tdms_path: str, slc: slice) -> np.array:
        """Read array from TDMS file using nptdms or binary handle.
        
        :param tdms_path: TDMS file path for reading
        :param slc: array index slice
        :return: array values from TDMS file
        """

        tdms_name, _ = os.path.splitext(tdms_path)
        csv_path = f"{tdms_name}_RawDataAddresses.csv"

        if os.path.isfile(csv_path):
            return self.read_binary(csv_path, tdms_path, slc)
        else:
            return self.read_nptmds(tdms_path, slc)

    def read_nptmds(self, tdms_path, slc):
        """Read array from TDMS file using nptdms.
        
        :param tdms_path: TDMS file path for reading
        :param slc: array index slice
        :return: array values from TDMS file
        """

        self._tdms_file = tdms_path
        rel_path = self.file_api.get_file(self._tdms_file)
        self._tdms = TdmsFile(rel_path)

        channel = self.board.channel(self.electrode)
        self._signal = self._tdms.object(channel[0], channel[1]).raw_data

        return self._signal[slc]


class ChunkSignal:
    """Lazy signal manager for multiple ChunkElectrodes."""

    def __init__(
        self,
        dir_path: Optional[str] = None,
        s3_id: Optional[int] = None,
        downsample: int = 1,
        save_cache: bool = True,
    ):
        """Create new ChunkSignal instance.

        :param dir_path: experiment directory path
        :param s3_id: rems-uploads S3 bucket id
        :param downsample: skip every (downsample -1) sample
        :param save_cache: save s3 cache after program exit
        """

        # try:
        self.file_api = FileAPI.COMPUTER
        self.board = self.file_api.board(dir_path)
        self.path_id = dir_path
        # except FileNotFoundError:
        #    self.file_api = FileAPI.S3_BUCKET
        #    self.board = self.file_api.board(s3_id)
        #    self.path_id = s3_id

        self.downsample = downsample
        self.sample_rate = self.board.sample_rate / self.downsample
        self.tdms_paths = self.find_tdms_paths()

        if len(self.tdms_paths) == 0:
            raise FileNotFoundError(f"no TDMS files found in {dir_path}")

        # Used for optimization and case checking
        self._current_file = ""

    def __getitem__(
        self, key: Union[int, Tuple[int, int]]
    ) -> Union[float, np.array]:
        """Get ChunkElectrode corresponding to electrode.

        An electrode's ChunkElectrode is recreated each time this function is called.
        As a result mutating the ChunkElectrode will not affect the ChunkElectrode
        returned from the next call of this function.

        :param key: electrode location (row, column) or column-major oriented integer 
        :return: ChunkElectrode
        """

        if isinstance(key, int):
            val = key // self.board.dimensions[1]
            electrode = (val, key - val)
        elif isinstance(key, tuple):
            electrode = key
        else:
            raise KeyError

        # 8CAB electrode row and columns are unfortunately inconsistent with results.mat files
        # switching the rows and columns
        if self.board == Board.EIGHTCAB and electrode[1] == 0:
            electrode = tuple(reversed(electrode))

        row_paths = self.get_row_paths(electrode[0])

        return ChunkElectrode(
            self.board, self.file_api, row_paths, electrode, self.downsample
        )

    def __len__(self) -> int:
        """Get signal array length of any electrode.

        All electrodes have the same signal length.

        :return: length
        """

        return len(self[0])

    def find_tdms_paths(self) -> Dict[Tuple[int, int], str]:
        """Retreive all result files in tuple key dictionary.
        
        :return: sorted dictionary with electrode locations as keys
        """

        glob_files = self.file_api.glob_files(self.board, self.path_id)
        regex = self.board.filename_regex
        index = lambda file_path: (
            *map(int, regex.match(file_path).group(1, 2)),
        )
        return {index(glob_file): glob_file for glob_file in glob_files}

    def get_row_paths(self, row: int) -> List[str]:
        """Get all TDMS chunk files for an chip row.

        :param row: row index
        :return: list of TDMS chunk files
        """

        min_time, max_time = (
            min(self.tdms_paths.keys())[1],
            max(self.tdms_paths.keys())[1],
        )
        row_paths = [
            self.tdms_paths[row, time_idx]
            for time_idx in range(min_time, max_time + 1)
        ]

        return row_paths

    def load_row(self, row: int, cols: Sequence[int]) -> List[np.array]:
        """Load entire time series for each column in a row efficiently.

        :param row: row
        :param cols: columns of requested time series
        :return: time series
        """

        row_paths = self.get_row_paths(row)

        n = len(cols)
        series = [[] for _ in range(n)]
        for path in row_paths:
            tdms = TdmsFile(path)
            chunk = [
                tdms.object("Frame Data", f"Row {row}, Column {k}").raw_data
                for k in cols
            ]
            for idx in range(n):
                series[idx] = np.append(series[idx], chunk[idx])

        return series


class Gen3row:
    def __init__(self, dir_path: str, row: int):
        self.dir_path = dir_path

        self.row = row

        data = {
            "dimensions": (64, 256),
            "filename_regex": re.compile(r"^.*_R(\d+)_(\d+)_DC\.tdms$"),
            "sample_rate": 1_000,
            "samples_per_chunk": 600_000,
            "time_per_chunk": 600.0,
        }

        for name, attr in data.items():
            setattr(self, name, attr)
        self.paths = self.find_tdms_paths()

        if len(self.paths) == 0:
            raise FileNotFoundError(f"no TDMS files found in {dir_path}")

        self._current_file = ""

        row_paths = [
            self.paths[self.row, time_idx]
            for time_idx in range(max(self.paths.keys())[1] + 1)
        ]

        self.pixels = [[] for _ in range(256)]
        for path in row_paths:
            chunk = self.read_tdms(path)
            for i in range(256):
                self.pixels[i] = np.append(self.pixels[i], chunk[i])

    def find_tdms_paths(self) -> Dict[Tuple[int, int], str]:
        glob_files = self.glob_files(self.dir_path)
        regex = self.filename_regex
        index = lambda file_path: (
            *map(int, regex.match(file_path).group(1, 2)),
        )
        return {index(glob_file): glob_file for glob_file in glob_files}

    def glob_files(self, dir_path: str) -> Iterator[str]:
        subdir_path = os.path.normpath(os.path.join(dir_path, "chunked_files"))
        return iglob(f"{subdir_path}/*DC.tdms")

    def read_tdms(self, path):
        if self._current_file != path:
            self._tdms_file = path
            self._tdms = TdmsFile(self._tdms_file)
            self.signal = [
                self._tdms.object(
                    "Frame Data", f"Row {self.row}, Column {i}"
                ).raw_data
                for i in range(256)
            ]
        return self.signal


class FileAPI(Enum):
    """API for file search and retrieval on multiple systems."""

    COMPUTER = "computer"
    # S3_BUCKET = "s3bucket"

    def __init__(self, system: str):
        """Create new FileAPI instance.

        Expected to only be created by ChunkSignal.

        :param system: file system
        """

        # if system == "s3bucket":
        #    self.rems_upload = REMSUpload()

    def board(self, path_id: Union[int, str]) -> "Board":
        """Determine experiment board from REMS_DATA directory path.
        
        :param path_id:
        :return: Board instance
        :raise: FileNotFoundError if path_id is a network path and does not exist
        """

        if self == self.COMPUTER:
            if not os.path.isdir(path_id):
                raise FileNotFoundError(path_id)

            gen3 = os.path.isdir(os.path.join(path_id, "chunked_files"))
        # elif self == self.S3_BUCKET:
        #    gen3 = bool([*self.rems_upload.search(path_id, "^.*chunked_files$"),])
        else:
            raise ValueError(f"unable to find {path_id}")

        return Board.GEN3 if gen3 else Board.EIGHTCAB

    def get_file(self, file_path: str) -> str:
        """Get local file path for reading.

        :param file_path:
        :return:
        """

        # if self == self.S3_BUCKET:
        #    return self.rems_upload.download(file_path)
        # else:
        return file_path

    def glob_files(self, board: Board, path_id: str) -> Iterator[str]:
        """Find all relevant TDMS file given root experiment directory path."""

        if self == self.COMPUTER:
            if board == Board.EIGHTCAB:
                return iglob(f"{path_id}/*_DC.tdms")
            else:
                subdir_path = os.path.normpath(
                    os.path.join(path_id, "chunked_files")
                )
                return iglob(f"{subdir_path}/*_DC.tdms")
        # elif self == self.S3_BUCKET:
        #    if board == Board.EIGHTCAB:
        #        return self.rems_upload.search(path_id, "^.*_DC.tdms$")
        #    else:
        #        return self.rems_upload.search(path_id, "^.*chunked_files/.*_DC.tdms$")
        else:
            raise ValueError


if __name__ == "__main__":
    # # 8CAB nptmds case
    # dir_path = r"R:\Data 1\2019\2019-10\2019-10-08\20191008T104834_8CAB-T2-011_W261-158"
    # chunk_signal = ChunkSignal(dir_path)
    # # Get length
    # n = len(chunk_signal)
    # print(f"length = {n}")
    # # Case of requesting signal from two TDMS files
    # signal = chunk_signal[3][2_999_980:3_000_020]
    # print(type(signal), len(signal), signal)

    # Gen3 nptdms case
    dir_path = r"\\fileserver2.roswellbt.com\GEN3DATA1\GEN3DATA1\Processed\2019\2019-10\2019-10-25\20191025T114847_GEN3-01_G3-739"
    chunk_signal = ChunkSignal(dir_path)
    resp = chunk_signal.load_row(4, [0, 6, 8])
    print(resp)
    print(resp[1])
