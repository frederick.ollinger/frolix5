import boto3
import os
from pathlib import Path
import re
from libCore.util.types import Gen


class REMSUpload:
    def __init__(self):
        """Setup S3 rems-upload bucket."""

        self.cache = []
        self.s3 = boto3.resource("s3")
        self.s3_client = boto3.client("s3")
        self.bucket = self.s3.Bucket("rems-uploads")
        self.reset()

    def download(self, bucket_path: str) -> str:
        """Download file if not already in local cache.

        :param bucket_path: file path in S3 bucket
        :return: file location in cache
        """

        rel_path = re.match("^.*uploads/experiments/(.*)$", bucket_path).group(
            1
        )
        cache_path = os.path.normpath(os.path.join(self.cache_dir, rel_path))

        dir_name = os.path.dirname(cache_path)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        if not os.path.exists(cache_path):
            self.cache.append(cache_path)
            self.s3_client.download_file(
                self.bucket.name, bucket_path, cache_path
            )

        return cache_path

    def search(self, s3_id: int, regex: str = "^.*$") -> Gen[str]:
        """Find all files in S3 experiment directory that satisfy a regular expression.

        :param s3_id: S3 experiment id
        :param regex: regular expression for file paths
        :yield: S3 file path
        """

        expr_path = f"uploads/experiments/{s3_id}"
        regex = re.compile(regex)

        for item in self.bucket.objects.filter(Prefix=expr_path):
            bucket_path = item.key
            if regex.match(bucket_path):
                yield bucket_path

    def reset(self):
        """Find cache location and create it if it does not exist."""

        while self.cache:
            cache_path = self.cache.pop(0)
            os.remove(cache_path)

        self.home_dir = str(Path.home())
        self.cache_dir = os.path.normpath(f"{self.home_dir}/.s3-cache")

        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)


if __name__ == "__main__":
    rems_upload = REMSUpload()
    files = rems_upload.search(32624, r"^.*DC\.tdms$")
    for file in files:
        cache_path = rems_upload.download(file)
        print(cache_path)
