import sys
import platform as plat


def bad_drive(drives):
    print(
        "\nError!! The drive entered is not known. Please use one of the following:"
    )
    for drive in drives:
        print(drive)


def path(drive: str):
    os_type = plat.system()

    if os_type == "Linux":
        prefix = "/mnt"
    elif os_type == "Darwin":
        prefix = "/Volumes"
    elif os_type == "Windows":
        if drive in ["remslocal", "remsdata"]:
            prefix = "//fileserver"
        else:
            prefix = "//fileserver2"
    else:
        sys.exit("Operating system not recognized.")

    drives = ["remslocal", "rems_data", "gen3data1", "gen3data2", "gen3data3"]

    if drive not in drives:
        sys.exit(bad_drive(drives))

    if os_type in ["Darwin", "Windows"]:
        return f"{prefix}/{drive.upper()}"
    else:
        if drive == "remslocal":
            return f"{prefix}/{drive}"
        elif drive == "rems_data":
            return f"{prefix}/8cab"
        else:
            return f"{prefix}/{drive[:4]}/{drive[4:]}"


if __name__ == "__main__":
    drives = ["remslocal", "rems_data", "gen3data1", "gen3data2", "gen3data3"]
    print("Success if we get paths from each drive.")
    for drive in drives:
        print(f"{drive} has path: {path(drive)}")

    foo = "foo"
    print("Success if the bad drive error prints.")
    print(f"foo has path: {path(foo)}")
