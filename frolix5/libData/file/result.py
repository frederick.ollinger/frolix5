from glob import iglob
from math import sqrt
import os
from pprint import pprint
import re
from typing import Dict, List, Optional, Sequence, Tuple
from libCore.device.bioSensor.sequencer.roi_finder import is_valid
from libCore.util.functools import take
from libCore.util.types import Gen, Pulse, ROI, T
from libData.file.roswellIO import loadmat
from libData.scrub.util import list_dict


def contains(x: Tuple[T, T], y: Tuple[T, T]) -> bool:
    """Check if y values are inside x values."""

    return x[0] < y[0] and y[0] < x[1]


def find_expr_dir(dir_path: str, expr: str) -> str:
    """Find directory which contains direct current data."""

    cond = lambda dir_name: dir_name.endswith(expr)
    expr_dirs = [*filter(cond, iglob(f"{dir_path}/*"))]

    regex = re.compile("^.*DC1_Event.log$")
    for expr_dir in expr_dirs:
        matches = [*filter(regex.match, os.listdir(expr_dir))]

        if matches:
            return expr_dir


def get_result_files(dir_path: str) -> Dict[int, str]:
    """Retreive all result files"""

    subdir_path = os.path.normpath(
        os.path.join(dir_path, "chunked_files/ROIfinder")
    )
    glob_files = iglob(f"{subdir_path}/*results.mat")

    get_row = lambda file_path: int(
        re.match(r"^.*R(\d+)_results\.mat$", file_path).group(1)
    )
    return {get_row(file_path): file_path for file_path in glob_files}


def get_features(file_path: str):

    results = list_dict(loadmat(file_path)["R"])

    result = results[2]
    electrode = {
        "column": result["col"],
        "electrode": int(result["electrode"]),
        "neg": result["neg"],
        "pos": result["pos"],
        "roi": result["ROI"],
        "row": result["row"],
    }

    return electrode


def get_pulses(
    result_files: Sequence[str],
    electrode: Tuple[int, int],
    irange: Optional[range] = None,
) -> Gen[Pulse]:

    result_file = result_files[electrode[0]]
    results = list_dict(loadmat(result_file)["R"])

    result = results[electrode[1]]
    irange = range(len(result["pos"]["mean"])) if irange is None else irange

    for idx in irange:
        getter = lambda key: result["pos"][key][idx]
        yield {
            "area": getter("area"),
            "idxStart": getter("f1"),
            "skewness": getter("kurtosis"),
            "max": getter("maxCurrent"),
            "mean": getter("mean"),
            "min": getter("minCurrent"),
            "sampleCnt": getter("df"),
            "skewness": getter("skewness"),
            "std": sqrt(getter("variance")),
            "time": (getter("f1") / 1000, getter("f2") / 1000),
        }


def get_rois(
    result_files: Sequence[str],
    electrode: Tuple[int, int],
    rois: List[int] = [],
) -> Gen[ROI]:

    result_file = result_files[electrode[0]]
    results = list_dict(loadmat(result_file)["R"])

    result = results[electrode[1]]
    meta = result["ROI"]
    times = meta["tRange"]

    if not times or isinstance(times[0], float):
        return

    rois = rois if rois else [*range(len(times[0]))]
    for roi in rois:
        time = (times[0][roi], times[1][roi])

        cond = lambda pulse: contains(time, pulse["time"])
        pulses = [*filter(cond, get_pulses(result_files, electrode))]
        yield {"pulse": pulses, "start": time[0], "stop": time[1]}


if __name__ == "__main__":
    dir_path = r"\\fileserver2.roswellbt.com\GEN3DATA1\GEN3DATA1\Processed\2019\2019-10\2019-10-25"
    expr = "GEN3-01_G3-739"
    expr_dir = find_expr_dir(dir_path, expr)
    result_files = get_result_files(expr_dir)

    rois = []
    for row in range(2, 64):
        for col in range(256):
            electrode = [row, col]
            electrode_rois = [
                is_valid(roi["pulse"])
                for roi in get_rois(result_files, electrode)
            ]
            rois += electrode_rois

        print(
            f"row {row} status: {sum(rois)} valid out of {len(rois)}, {sum(rois) / len(rois)}"
        )

    print(f"valid: {sum(rois)} out of {len(rois)}, {sum(rois) / len(rois)}")
