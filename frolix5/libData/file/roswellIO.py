import numpy as np
import scipy.io as spio
import pandas as pd
import click

from nptdms import TdmsFile
from itertools import product


def loadmat(filename):
    """
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects

    from: `StackOverflow <http://stackoverflow.com/questions/7008608/scipy-io-loadmat-nested-structures-i-e-dictionaries>`_
    """
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)


def _check_keys(dict):
    """
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    """
    for key in dict:
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
    return dict


def _todict(matobj):
    """
    A recursive function which constructs from matobjects nested dictionaries
    """
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        else:
            dict[strg] = elem
    return dict


def get_8cab_traces(tdms_file, t, rows):
    traceDict = {}
    electrodes = ["Electrode {}".format(str(int(row))) for row in rows]
    for electrode in electrodes:
        traceDict[electrode] = tdms_file.object(
            "8CAB_Active_Bridging", electrode
        ).data
    traceDict["time"] = (
        tdms_file.object("8CAB_Active_Bridging", electrode).time_track() + t
    )
    return pd.DataFrame(data=traceDict)


def get_gen3_traces(tdms_file, t, rows, columns):
    traceDict = {}
    pixels = [
        "Row {}, Column {}".format(str(int(row)), str(int(column)))
        for row, column in product(rows, columns)
    ]
    for pixel in pixels:
        traceDict[pixel] = tdms_file.object("Frame Data", pixel).data
    traceDict["time"] = t + np.linspace(
        0, len(traceDict[pixel]), len(traceDict[pixel])
    )
    return pd.DataFrame(data=traceDict)


def load_trace_data(instrument, chunks, rows, columns=None):
    """
    Uses instrument, chunks, rows, and optionally columns to load tdms file traces
    into a pandas datafarme.

    Parameters
    ----------
    instrument: str
        8cab or gen3 for now.

    chunks: list of str
        paths to all of the chunks of interest.

    rows: int or list of ints
        electrode (8CAB) or row (GEN3) numbers.

    columns: int or list of interest
        column numbers, GEN3 only.

    Returns
    -------
    df: pandas DataFrame
        contains all of the trace data (default units) + time (secondes on 8CAB,
        sample points on GEN3)
    """
    instrument = instrument.lower()
    df = pd.DataFrame()
    t = 0
    if "8cab" in instrument:
        with click.progressbar(chunks) as bar:
            for chunk in bar:
                tdms_file = TdmsFile(chunk)
                df = df.append(get_8cab_traces(tdms_file, t, rows))
                t = df.time.iloc[-1]
            return df
    elif "gen3" in instrument:
        with click.progressbar(chunks) as bar:
            for chunk in bar:
                tdms_file = TdmsFile(chunk)
                df = df.append(get_gen3_traces(tdms_file, t, rows, columns))
                t = df.time.iloc[-1]
            return df
    else:
        return print(
            "Error in instrument name provided. Please use either '8CAB' or 'GEN3'"
        )
