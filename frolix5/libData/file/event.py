from datetime import datetime
import re
from typing import Dict, List, Optional, Tuple
import matplotlib.pyplot as plt


def parse(file_path: str) -> List[Tuple[datetime, Optional[str], str]]:
    """Split event log into timestamp tuples with phase and message information.

    :param file_path: event log
    :return: list of tuples formatted as (timestamp, phase, message)
    """

    with open(file_path, "r") as handle:
        events = [parse_line(line) for line in handle]

    return events


def parse_line(line: str) -> Tuple[datetime, Optional[str], str]:
    """Split line into timestamp, phase, and message information

    :param line: line for parsing
    :return: tuple formatted (timestamp, phase, message)
    """

    regex = re.compile(
        r"""
        ^(?P<timestamp>(\d{2}/?){3}\s(\d{2}:?){3})\s+
        (\#(?P<phase>[\w-]+)\s)?
        (?P<message>.*)$
        """,
        re.VERBOSE,
    )
    match = regex.match(line)

    timestamp = datetime.strptime(
        match.group("timestamp"), r"%m/%d/%y %H:%M:%S"
    )
    phase = match.group("phase")
    message = match.group("message")
    return timestamp, phase, message


def phase_msgs(file_path) -> List[Tuple[datetime, Optional[str], str]]:
    """Return only phase messages from event log.

    :param file_path: event log
    :return: list of tuples formatted as (timestamp, phase, message)
    """

    not_none = lambda tupl: tupl[1] is not None
    return list(filter(not_none, parse(file_path)))


# Return a list of phases with their start and end times.
def phases(file_path, time_zero_at_exp_start=True):
    msgs = phase_msgs(file_path)
    timeZero = msgs[0][0]

    # allow setting reference point to "Experiment Start." time point
    if time_zero_at_exp_start:
        parsed_logfile = parse(file_path)
        for line in parsed_logfile:
            if "experiment start" in line[2].lower():
                timeZero = line[0]

    start = -1
    end = -1
    last_msg_type = "IGNORE"
    last_msg = ""
    phase_list = []
    for msg in msgs:

        if msg[1] == "IGNORE":

            # If this is not the last seen message, this ends
            # a phase, so record it
            if last_msg_type is not "IGNORE":
                end = (msg[0] - timeZero).total_seconds()
                phase_list.append([last_msg_type, start, end, last_msg])

            start = (msg[0] - timeZero).total_seconds()
            last_msg_type = "IGNORE"

        elif msg[1] == "RESUME":

            # if the last message was 'IGNORE', record the ignore phase.
            if last_msg_type is "IGNORE":
                end = (msg[0] - timeZero).total_seconds()
                phase_list.append([last_msg_type, start, end, msg[2]])
            start = (msg[0] - timeZero).total_seconds()
            last_msg_type = "RESUME"

        elif msg[1] == "SETUP":
            last_msg_type = "SETUP"
        elif msg[1] == "SEQUENCING":
            last_msg_type = "SEQUENCING"
        elif msg[1] == "BASELINE" or msg[1] == "WET":
            last_msg_type = "BASELINE"
        elif msg[1] == "CONJUGATION":
            last_msg_type = "CONJUGATION"
        elif msg[1] == "NOTE":
            last_msg_type = "NOTE"

        last_msg = msg[2]

    return phase_list


if __name__ == "__main__":
    event_path = r"R:\Data 1\2019\2019-11\2019-11-14\20191114T110151_8CAB-T2-015_W277-3\8CABT2-015_W277-3_20191114T110151_SI-None-T30-dA05-06V-1_Event.log"
    events = phase_msgs(event_path)
    print(events)
