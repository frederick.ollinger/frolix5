#!/usr/bin/env python3
"""
@author: james.mullinix

LayoutParser: a Python3 class for parsing an Excel XLS(X/B) file containing the
              layout of a chip's pixel configurations into a JSON file
              
Usage example:
    from LayoutParser import LayoutParser
    parser = LayoutParser(file='SOME_FILE.xlsb')
    parser.read_file()
    parser.export_json()
    parser.json2db()
    
Input:
    file: file name in to read

Output:
    file: json file containing pixel layout information

#############    
TODO: Extract XLS[XB] to JSON parser to it's own class
#############

"""
GENERATION = "gen3"


class LayoutParser:
    configuration = {"gen3": {"rows": 64, "cols": 256}}

    def __init__(self, cli=False, generation=GENERATION, file=""):
        if cli:
            self.parse_cli()
        else:
            import re

            self.generation = generation
            self.file = file
            regex = re.compile(".xls[xb]{1}$", re.IGNORECASE)
            self.output_file = regex.sub("", self.file)
        self.rows = self.configuration[self.generation]["rows"]
        self.cols = self.configuration[self.generation]["cols"]

    def parse_cli(self):
        import argparse
        import re

        parser = argparse.ArgumentParser(
            description="Convert chip layout                                                       xlsb file to json."
        )
        parser.add_argument(
            "-f", "--file", help="file to read in", required=True
        )
        parser.add_argument(
            "-g",
            "--generation",
            help="chip generation",
            required=False,
            default=GENERATION,
            choices=(self.configuration.keys()),
        )
        parser.add_argument(
            "-o",
            "--output_file",
            help="output file name",
            required=False,
            default="",
        )
        args = parser.parse_args()

        self.file = args.file
        self.generation = args.generation
        if args.output_file == "":
            regex = re.compile(".xls[xb]{1}$", re.IGNORECASE)
            self.output_file = regex.sub("", self.file)
        else:
            regex = re.compile(".json$", re.IGNORECASE)
            self.output_file = regex.sub("", args.output_file)

    def read_file(self):
        from pyxlsb import open_workbook

        with open_workbook(self.file) as wb:
            names = wb.sheets

        data = [dict() for i in range(self.cols * self.rows)]

        with open_workbook(self.file) as wb:
            for name in names:
                with wb.get_sheet(name) as sheet:
                    ix = 0
                    row_ix = 0
                    for row in sheet.rows():
                        col_ix = 0
                        if row_ix >= self.rows:
                            break
                        for col in row:
                            if col_ix >= self.cols:
                                break
                            data[ix][name] = int(col.v)
                            ix += 1
                            col_ix += 1
                        row_ix += 1

        self.pixels = data

    def pixels2json(self):
        import jsonpickle

        self.json = jsonpickle.encode(
            {"filename": self.output_file, "pixels": self.pixels}
        )

    def export_json(self):
        self.pixels2json()
        with open(self.output_file + ".json", "w") as (fh):
            fh.write(self.json)

    def json2db(self):
        import requests

        with requests.Session() as s:
            http_url = "https://rems.roswellbt.com/rems/Account/passwordLogin"
            http_url += "?" + "email=PythonUser@roswellbiotech.com"
            http_url += "&" + "password=A!ienT@chn0logy"
            response = s.get(http_url)
            print("Login: ", response.text)
            header = {
                "Content-type": "application/json",
                "Accept": "text/plain",
            }
            http_url = "https://rems.roswellbt.com/rems/"
            http_url += "api/webapi/setChipPixelDefinition"
            response = s.post(http_url, data=self.json, headers=header)
            print("API response: " + response.text)


if __name__ == "__main__":
    parser = LayoutParser(cli=True)
    parser.read_file()
    parser.export_json()
