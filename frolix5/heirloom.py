"""Legacy functions for creating H5 files.

All these functions are deprecated and slated for removal.
"""
import ast
import datetime as dt
import os
import re
from typing import Any, Dict, List

import numpy as np

import pandas as pd

import rbtdata.expr.gen3.search as search
from rbtdata.expr.h5.phs import H5Phase


def getDatetime(exp_dir):
    """Parse exp_dir for the datetime."""
    base_dir = os.path.basename(exp_dir)  # \date_inst_chip
    ls = base_dir.split("_")  # [date, inst, chip]
    datetime = ls[0]
    if len(datetime) != 15:  # YYYYMMDDTHHMMSS
        raise ValueError("Datetime not in 15 digit format.")
    return datetime


def _get_exp_id(exp_dir):
    """Parse exp_dir for the system (G3A) and chip number."""
    exp_id = re.search(r"_(G\d\w?)\-(\d+)$", exp_dir)
    system = exp_id[1]
    chip = exp_id[2]
    return system, chip


def get_elec_geom(chipmap: str, row: int, col: int):
    """For a given chip map version, row, and column.

    return the gap size and shape of the electrode
    Args:
        chipmap: path to the chipmap
        row/col: position on the chip
    Returns:
        size:    electrode gap in nm
        shape:   tip shape (blunt or pointy)
    """
    df = pd.read_excel(chipmap, index_col=0)
    metrics = ast.literal_eval(df[col][row])
    size = metrics["size"]
    shape = metrics["shape"]
    return size, shape


def get_pixels(path_to_sheet: str, sheets: List[str] = ["Of Interest"]):
    """Read in pixels from Excel files produced.

    By Kevin's tool and converts them into the list
    of tuples the create_h5 function already took.

    Args:  path_to_sheet of curated pixels
    Returns: pixels -> a list of tuples of row/col pairs
    """
    pixels = []
    for sheet in sheets:
        data = pd.read_excel(path_to_sheet, sheet_name=sheet)
        for idx, row in data.iterrows():
            try:
                row = int(data["row"][idx])  # manual
            except KeyError:
                row = int(data["Row"][idx])  # Kevin's tool
            try:
                col = int(data["Column"][idx])  # manual
            except KeyError:
                col = int(data["Col"][idx])  # Kevin's tool
            pixel = (row, col)
            pixels.append(pixel)

    return pixels


def get_filename(exp_dir):
    """Create H5 output filename.

    Args: exp_dir
    Returns: h5 filename based on chip ('G3A-1745.h5')
    """
    system, chip = _get_exp_id(exp_dir)
    filename = system + "-" + chip + ".h5"  # add hyphen and h5 extension
    return filename


def get_phases() -> List[H5Phase]:
    """Get Phase Info."""
    pstart: dt.datetime = dt.datetime.fromtimestamp(1586117448)
    pstop: dt.datetime = dt.datetime.fromtimestamp(1586117875)

    name = "phase[00]"

    phase_dict: Dict[str, Any] = {
        "message": "AB solution, 5 min. ",
        "phase_name": "CONTROL",
        "t_start": np.array([-1000.0]),
        "t_stop": np.array([427000.0]),
        "reagent_group": {
            "MgCl2": 10,
            "PEXmREX": 0.01,
            "Primer-template": 0.01,
            "dTTP": 10,
        },
    }

    return [H5Phase(pstart, pstop, name, phase_dict)]


def get_exp_dir(paths):
    """Find the directory which has DC.

    Arguments:
        paths - A list of PosixPaths.
    This is our exp_dir, return it.
    """
    for path in paths:
        tmp = search.dir_phase(path)
        if "DC" == tmp:
            return path

    raise IOError


def validate_nucleotide(nuc: str) -> bool:
    """Ensure that our nucleotide field is valid."""
    if re.search(r"^d(A|C|G|T)\SP", nuc):
        return True
    return False
