import numpy
from abc import ABC, abstractmethod

import frolix5.pixelio.pixel as pix


class PixelReader(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def read(self, row: int, col: int) -> pix.Pixel:
        pass
