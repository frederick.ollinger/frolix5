import numpy
import frolix5.pixelio.pixel as pix


class PixelList:
    def __init__(self) -> None:
        self._rows = numpy.empty(0)
        self._cols = numpy.empty(0)
        # TODO IMPLEMENT
        self._sparse = numpy.empty(0)
        self._phase_boundaries = numpy.empty(0)
        self._addresses = numpy.empty(0)
        self._pixels = []

    # Class Properties
    @property
    def rows(self) -> numpy.ndarray:
        return self._rows

    @property
    def cols(self) -> numpy.ndarray:
        return self._cols

    @property
    def pixels(self) -> list:
        return self._pixels

    def add_pixel(self, p: pix.Pixel) -> None:
        self._pixels.append(p)
        numpy.append(self._rows, p.row)
        numpy.append(self._cols, p.col)

    def __getitem__(self, key):
        return self._pixels[key]

    # Class Methods
    def __len__(self):
        return len(self._pixels)

    def __str__(self):
        for p in self._pixels:
            print(p)
