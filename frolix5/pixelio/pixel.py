import numpy


class Pixel:
    def __init__(self) -> None:
        self._idx = 0
        self._data = numpy.empty(0)
        self._row = 0
        self._col = 0
        self._sparse = True

    # Class Properties
    @property
    def row(self) -> int:
        return self._row

    @row.setter
    def set_row(self, row: int) -> None:
        self._row = row

    @property
    def col(self) -> int:
        return self._col

    @col.setter
    def set_col(self, col: int) -> None:
        self._col = col

    @property
    def idx(self) -> int:
        return self._idx

    @idx.setter
    def set_idx(self, idx: int) -> None:
        self._idx = idx

    @property
    def sparse(self) -> bool:
        return self._sparse

    @sparse.setter
    def set_sparse(self, sparse: bool) -> None:
        self._sparse = sparse

    @property
    def data(self, start_idx=0, stop_idx=-1) -> numpy.ndarray:
        print("Pixel.data() STUB")
        return self._data

    @data.setter
    def set_data(self, data: numpy.ndarray) -> None:
        self._data = data

    # Class Methods
    # TODO RETURN A SUBSET OF _data as str, also
    def __str__(self):
        return f"{self._row}, {self.col}"
