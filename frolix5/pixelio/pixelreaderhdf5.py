import h5py, numpy, pathlib

import frolix5.pixelio.pixel as pix
import frolix5.pixelio.pixelreader as PixelReader
import frolix5.pixelio.pixellist as pixlist


class PixelReaderHDF5(PixelReader.PixelReader):
    def __init__(self) -> None:
        _key = ""
        _path = ""

    def __init__(self, key, path) -> None:
        self._key = key
        self._path = path

    # Class Properties
    @property
    def key(self) -> pathlib.Path:
        return pathlib.Path(self._key)

    @key.setter
    def set_key(self, key: str) -> None:
        self._key = key

    @property
    def path(self) -> pathlib.Path:
        return pathlib.Path(self._path)

    @path.setter
    def set_path(self, path: str) -> None:
        self._path = path

    # Class Method
    def read(self, row: int, col: int) -> pix.Pixel:
        fp = pathlib.Path.joinpath(self.key, self.path)
        h5 = h5py.File(fp, mode="r")
        p = pix.Pixel()
        for i in range(len(h5["pixel_info"])):
            pixel = "pixel[{}]".format(i)
            if (
                row == h5["pixel_info"][pixel]["row"][()].item()
                and col == h5["pixel_info"][pixel]["col"][()].item()
            ):
                p.set_data = h5["pixel_info"][pixel]["raw_signal"][()]
                p.set_idx = pixel
                p.set_row = row
                p.set_col = col
                return p

        return p

    def read(self) -> pixlist.PixelList:
        fp = pathlib.Path.joinpath(self.key, self.path)
        h5 = h5py.File(fp, mode="r")
        lst = pixlist.PixelList()
        for i in range(len(h5["pixel_info"])):
            p = pix.Pixel()
            pixel = "pixel[{}]".format(i)
            p.set_data = h5["pixel_info"][pixel]["raw_signal"][()]
            p.set_idx = pixel
            p.set_row = h5["pixel_info"][pixel]["row"][()].item()
            p.set_col = h5["pixel_info"][pixel]["col"][()].item()
            lst.add_pixel(p)

        return lst
