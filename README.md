# frolix5

HDF5 Reading/Writing Library

## Windows QuickStart

Be in Powershell only.

1. Clone the repo onto C:\frolix5

NOTE: IF YOU DON'T CLONE RECURSIVE THEN YOUR CONTAINER WILL BE TOTALLY BROKEN.

DELETE C:\frolix5 AND START OVER.

```
git clone --recursive git@gitlab.com:frederick.ollinger/frolix5.git 'C:\frolix5'
```

2. login to gitlab container registry

```
docker login registry.gitlab.com
```

NOTE: Use personal token to login instead of password. See gitlab docs for details on personal access tokens.

3. Run batch script

```
cd C:\frolix5\docker
.\biobench_jupyter.bat
```

* If you have your repo elsewhere just put that as the first cli arg:

```
biobench_jupyter.bat path_to_frolix5
```

4. Cut n' paste "last" url (127.0.0.0 something URL) line into a web browser.

5. In the web browser, open a Terminal by clicking on the button at the bottom of the page.

NEXT STEPS ARE INSIDE THE CONTAINER

6. Open a real shell:

```
bash
```

7. One time setup might need to be done. Trying to eliminate but for now, do this 1st time:

```
./bootstrap.sh
```

8. Edit autobahn.yml file.

From the side panel, you can edit the script for autobahn.yml and add your specific chip and reagent information.

9. Create the HDF5 File.

This is the reason we are here. If we have gotten this far, pat yourself on the back.

```
cd
./autobahn.py
```

NOTE: The autobahn.yml file will modify what changes you need to do in order to run the script.

You can also add a second yaml file if you want to run from a different file:

```
.\autobahn.py custom_config.yml
```

```

## Updating

There's an update script which will do all the work. Run this script inside the Jupyter terminal:

```
./update.sh
```

## Troubleshooting

1. The most common issue is the network share breaking. The vast majority of my labor was in getting this working out of the box.

If you have a terminal, you can do

```
mount
```

Part of the output should include:

```
//172.20.20.18/GEN3DATA1 on /mnt/gen3/data1 type cifs (rw,nosuid,nodev,relatime,vers=default,sec=ntlmssp,cache=strict,username=spdev.sa,domain=roswellbt.com,uid=1000,forceuid,gid=1000,forcegid,addr=172.20.20.18,file_mode=0777,dir_mode=0777,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,user=developer)
//172.20.20.18/GEN3DATA2 on /mnt/gen3/data2 type cifs (rw,nosuid,nodev,relatime,vers=default,sec=ntlmssp,cache=strict,username=spdev.sa,domain=roswellbt.com,uid=1000,forceuid,gid=1000,forcegid,addr=172.20.20.18,file_mode=0777,dir_mode=0777,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,user=developer)
//172.20.20.18/GEN3DATA3 on /mnt/gen3/data3 type cifs (rw,nosuid,nodev,relatime,vers=default,sec=ntlmssp,cache=strict,username=spdev.sa,domain=roswellbt.com,uid=1000,forceuid,gid=1000,forcegid,addr=172.20.20.18,file_mode=0777,dir_mode=0777,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,user=developer)
//172.20.20.16/REMS_DATA on /mnt/8cab type cifs (rw,nosuid,nodev,relatime,vers=default,sec=ntlmssp,cache=strict,username=spdev.sa,domain=roswellbt.com,uid=1000,forceuid,gid=1000,forcegid,addr=172.20.20.16,file_mode=0777,dir_mode=0777,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,user=developer)
//172.20.20.16/REMSLOCAL on /mnt/remslocal type cifs (rw,nosuid,nodev,relatime,vers=default,sec=ntlmssp,cache=strict,username=spdev.sa,domain=roswellbt.com,uid=1000,forceuid,gid=1000,forcegid,addr=172.20.20.16,file_mode=0777,dir_mode=0777,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,user=developer)
```

If it does not then there are issues with your login information.


# Design Document

The most up to date design document is on Confluence:

```
https://roswellbiotech.atlassian.net/wiki/spaces/SP/pages/347439177/Pixel+Reader+Writer+Design+Document
```

# File Format

```
https://roswellbiotech.atlassian.net/wiki/spaces/AI/pages/131236533/GEN3
```

# Notes

```
https://roswellbiotech.atlassian.net/wiki/spaces/SP/pages/336888547/Project+Plan
```

# Required Files 

```
\\fileserver\REMSLOCAL\Users\signal_processing\machine_learning\H5
```

# Notes

Data I/O interface to curated HDF5 file format; Will first write confluence spec and architecture design to review.

HDF5 file data I/O from code examples provided by Alex/Zach 

