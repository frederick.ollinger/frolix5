"""Reading from REMS Database."""
from typing import Any, List, Tuple

import mysql.connector


class RemsDb:
    class DBMan:
        """Wrapper to MySQL"""

        def __init__(
            self, user: str, passwd: str, host: str, database: str,
        ) -> None:
            self._user: str = user
            self._passwd: str = passwd
            self._host: str = host
            self._database: str = database

        def __enter__(self):
            self.database = mysql.connector.connect(
                host=self._host,
                user=self._user,
                passwd=self._passwd,
                database=self._database,
            )

            cursor = self.database.cursor()
            return cursor

        def __exit__(self, *exc):
            self.database.close()

    """REMS Database Connector."""

    def __init__(
        self,
        user: str,
        passwd: str,
        gds_version_id: str = "2",
        host: str = "rems.roswellbt.com",
        database: str = "remsdb",
    ) -> None:
        """Create Rems instance."""
        # cached row, col, size, shape
        # data from pixel_definitions table
        self._data: List[Tuple] = []
        # cached blunt, pointy data
        # from nanoelectrode_shape table

        # dimensions of the database
        self.ROW_MAX: int = 64
        self.COL_MAX: int = 256

        # Offsets based on the order we query
        self.ROW_IDX: int = 0
        self.COL_IDX: int = 1
        self.SIZE_IDX: int = 2
        self.SHAPE_IDX: int = 3

        # GDS_VERSION_ID is the version of the chip map we are on
        self._QUERY_STRING: str = f"Select pixel_row, pixel_col, \
            gap_size_nm, nanoelectrode_shape.name \
            as tip_shape \
            from pixel_definitions \
            join nanoelectrode_shape on \
            pixel_definitions.nanoelectrode_shape_id \
                = nanoelectrode_shape.id \
            join gds_version on \
            pixel_definitions.gds_version_id \
                = gds_version.id \
            where gds_version.name = {gds_version_id} \
            order by pixel_col, pixel_row;"

        self._user: str = user
        self._passwd: str = passwd
        self._host: str = host
        self._database: str = database

    @property
    def data(self) -> List[Tuple]:
        """Return raw size/shape data."""
        if 0 == len(self._data):
            self._data = self._query(self._QUERY_STRING)
        return self._data

    def get_size(self, row: int, col: int) -> Any:
        """Return size information."""
        return self.data[(self.ROW_MAX * col) + row][self.SIZE_IDX]

    def get_shape(self, row: int, col: int) -> Any:
        """Return tip shape information."""
        return self.data[(self.ROW_MAX * col) + row][self.SHAPE_IDX]

    def _query(self, query: str) -> Any:
        """Return query results."""
        with self.DBMan (
            host=self._host,
            user=self._user,
            passwd=self._passwd,
            database=self._database,
        ) as cursor_object:

            cursor_object.execute(query)
            result = cursor_object.fetchall()
        return result
