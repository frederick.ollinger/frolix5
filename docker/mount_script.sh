#!/usr/bin/env bash
# setup samba/cifs file shares
mkdir -p /mnt/gen3/data{1..3}
mkdir -p /mnt/8cab
mkdir -p /mnt/remslocal

for i in $(seq 1 3); do
        echo "//172.20.20.18/GEN3DATA$i /mnt/gen3/data$i cifs    credentials=/etc/smb_secrets,sec=ntlmssp,file_mode=0777,dir_mode=0777,rw,uid=1000,gid=1000,user    0      0" >> /etc/fstab
done
echo "//172.20.20.16/REMS_DATA  /mnt/8cab       cifs    credentials=/etc/smb_secrets,sec=ntlmssp,file_mode=0777,dir_mode=0777,rw,uid=1000,gid=1000,user    0      0" >> /etc/fstab
echo "//172.20.20.16/REMSLOCAL  /mnt/remslocal       cifs    credentials=/etc/smb_secrets,sec=ntlmssp,file_mode=0777,dir_mode=0777,rw,uid=1000,gid=1000,user    0      0" >> /etc/fstab

