@echo off
SETLOCAL ENABLEEXTENSIONS
SET image=registry.gitlab.com/frederick.ollinger/frolix5
SET port=7071
IF "%1"=="" (
	SET source_dir=C:\frolix5\
) ELSE (
	SET source_dir=%1
)
SET command=docker run --privileged -it --rm -e JUPYTER_RUNTIME_DIR=/tmp/runtime -v %source_dir%:/home/developer -w /home/developer -p %port%:%port% %image% /bin/bash -c "jupyter lab --ip=0.0.0.0 --port=%port% --allow-root"
docker login registry.gitlab.com
echo %command%
%command%
ENDLOCAL
