# Docker Container

## Rebuild the container

DO THESE STEPS ONLY IF YOU'RE A CONTAINER DEVELOPER WHO IS MAKING CHANGES
TO THE DOCKER CONTAINER.

IF THIS DOESN'T MAKE SENSE, YOU DON'T NEED TO DO THIS.

1. Populate smb_secrets with Windows credentials in this directory.

File format is as follows:

```
username=
password=
domain=roswellbt.com

```

2. Rebuild the container. (Yes, you need to have 'make' installed.)

```
make build
```

3. Push to gitlab container repo. Yes, you need to be logged in to gitlab.

```
make push
```
