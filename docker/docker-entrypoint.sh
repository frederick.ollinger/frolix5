#!/usr/bin/env bash

######## samba credential installation #########

### METHOD 1: ask user for credentials ###
#echo "initializing samba credentials..."
#user=$RBTU
#pass=$RBTP
#unset RBTU
#unset RBTP

#read -p 'Username: ' user
#read -sp 'Password: ' pass
# # echo a newline for prettiness
#echo ""
# write the results to /home/developer/.smb_secrets
#cat << END_HEREDOC > /home/developer/.smb_secrets
#username=$user
#password=$pass
#domain=roswellbt.com
#END_HEREDOC
# # set permissions to owner only read only
#chmod 400 /home/developer/.smb_secrets
### METHOD 2: Mack's script ??? ###

mount /mnt/gen3/data1
mount /mnt/gen3/data2
mount /mnt/gen3/data3
mount /mnt/8cab
mount /mnt/remslocal

exec "$@"
