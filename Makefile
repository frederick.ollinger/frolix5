USER=frederick.ollinger
# HOME is home directory to mount
HOME="//c/Users/${USER}/cygwin/home/${USER}/src/frolix5-trevor"

DOCKER=frolix5

DATAIN1="//c/Users/${USER}/cygwin/home/${USER}/src/datain"
DATAIN2="/opt/datain"

DATAOUT1="//c/Users/${USER}/cygwin/home/${USER}/src/dataout"
DATAOUT2="/opt/dataout"

.PHONY: test

all: test

# Run functional test in a Docker container
docker_test:
	docker run -it --rm --name hf5test ${DOCKER}
#	docker run -v ${DATAIN1}:${DATAIN2} -v ${DATAOUT1}:${DATAOUT2} -it --rm --name cnntest ${DOCKER}
#	docker run -v ${DATAIN1}:${DATAIN2} -v ${DATAOUT1}:${DATAOUT2} -it --rm --name rbtaitest ${DOCKER}

dev:
	docker run --env-file=env.list -u root -it --rm --privileged --name frolix5dev ${DOCKER} /bin/bash
#	docker run -u root -v ${DATAIN1}:${DATAIN2}:ro -v ${DATAOUT1}:${DATAOUT2}:rw -it --rm --name rbtaidev ${DOCKER} /bin/bash

build:
	docker build --tag ${DOCKER} -f Dockerfile .

prune:
	docker image prune -a

clean:
	rm -f /data/data/G3A-1692-out.h5

# inside the container run the test
#test: clean
	# python3 chip_hdf5.py G3A-1806.h5
#	cd rbtdata && python3 test-reader-writer.py

install:
	cd rbtdata && python3.7 -m pip install . --user
	#cd rbtdata && poetry run pip install .


oldpipeline:
	#docker build --tag oldpipeline -f Dockerfile_op .
	docker run -v /mnt:/mnt -it --rm -v /dev/shm:/dev/shm -u root -it --rm --name op_dev oldpipeline /bin/bash

static:
	black *.py
	cd rbtdata && mypy *.py



setup:
    # fileserver mounts
	rm -f ~/.smb_secrets
	cp smb_secrets ~/.smb_secrets
	chmod 0400 ~/.smb_secrets
	-./docker-entrypoint.sh
	# setup environment
	# copy shell defaults
	cp .bashrc ~/.bashrc
	# setup crypto for git
	mkdir -p ~/.ssh
	cp /data/.ssh/* ~/.ssh
	chmod 600 ~/.ssh/*

jupyter:
	docker run -it --rm --privileged -e JUPYTER_RUNTIME_DIR=/tmp/runtime -v ${HOME}:/home/developer -w /home/developer -p 7071:7071 ${DOCKER} /bin/bash -c "jupyter lab --ip=0.0.0.0 --port=7071 --allow-root"

