"""Compare local chipmap with database."""

import ast

from remsdb import RemsDb

import pandas as pd

import yaml


def parse_chip_map(chipmap: pd.core.frame.DataFrame, row: int, col: int):
    """Return size and shape from Excel Data."""
    metrics = ast.literal_eval(df[col][row])
    size = metrics["size"]
    shape = metrics["shape"]
    return row, col, size, shape


def assert_row_column_order(db):
    """Assert queries return from DB with proper order."""
    for col in range(db.COL_MAX):
        for row in range(db.ROW_MAX):
            result = db.data[(db.ROW_MAX * col) + row]
            assert row == result[db.ROW_IDX]
            assert col == result[db.COL_IDX]


def assert_size_shape(db, df):
    """Compare sizes and shapes in Excel vs. Database."""
    # Loop through all the row, col and compare the
    # size/shape with the database.
    for row in range(df.shape[0]):
        for col in range(df.shape[1]):
            db_size = db.get_size(row, col)
            db_shape = db.get_shape(row, col)
            _, _, size, shape = parse_chip_map(df, row, col)
            assert size == db_size
            assert shape == db_shape


CONFIG = "config/chip_map.yml"

with open(CONFIG) as config_file:
    config = yaml.safe_load(config_file)

db = RemsDb(config["mysql"]["username"], config["mysql"]["password"])
df = pd.read_excel(config["chip_map"], index_col=0)

assert_row_column_order(db)
assert_size_shape(db, df)
